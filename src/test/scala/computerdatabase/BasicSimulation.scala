package computerdatabase

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

class BasicSimulation extends Simulation {

  // Setup

  val httpConf: HttpProtocolBuilder = http
    .baseURL("http://localhost:8080")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
    .basicAuth("admin", "admin")

  // Scenarios

  val createAndTest: ScenarioBuilder = scenario("Create Users/Sites")

    .repeat(1) {

      exec(
        scenario("Create Users/Sites")
          .repeat(5) {
            exec(http("Create")
              .get("/alfresco/service/poc/create")
              .check(status.is(200))
            )
          }
          .repeat(10) {
            exec(http("Get")
              .get("/alfresco/service/poc/find")
              .check(status.is(200))
            )
          }
      )
    }

  val clean: ScenarioBuilder = scenario("Clean")
    .repeat(1) {
      exec(http("Cleanup")
        .get("/alfresco/service/poc/cleanup")
        .check(status.is(200))
      )
    }

  // Start

  val allScenarios: ScenarioBuilder = clean.exec(createAndTest).exec(clean)
  setUp(allScenarios.inject(atOnceUsers(1)).protocols(httpConf))

}
