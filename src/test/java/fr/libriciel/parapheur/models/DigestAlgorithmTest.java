/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.models;

import org.junit.Assert;
import org.junit.Test;


public class DigestAlgorithmTest {

    @Test public void getStringValue() throws Exception {

        Assert.assertEquals(DigestAlgorithm.MD5.toString(), "MD5");
        Assert.assertEquals(DigestAlgorithm.SHA1.toString(), "SHA-1");
        Assert.assertEquals(DigestAlgorithm.SHA256.toString(), "SHA-256");
    }

}