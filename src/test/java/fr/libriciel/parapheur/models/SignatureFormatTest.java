/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.models;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SignatureFormatTest {


    @Test public void getName() throws Exception {
        for (SignatureFormat signatureFormat : SignatureFormat.values()) {
            Assert.assertNotNull(signatureFormat.name);
            Assert.assertTrue(signatureFormat.toString().length() > 0);
        }
    }

    @Test public void fromString() throws Exception {

        Assert.assertEquals(SignatureFormat.CMS_PKCS7, SignatureFormat.fromString("PKCS#7/single"));
        Assert.assertEquals(SignatureFormat.CMS_PKCS7_Ain1, SignatureFormat.fromString("PKCS#7/multiple"));
        Assert.assertEquals(SignatureFormat.PADES_ISO32000_1, SignatureFormat.fromString("PAdES/basic"));
        Assert.assertEquals(SignatureFormat.PADES_ISO32000_1, SignatureFormat.fromString("PAdES/basicCertifie"));
        Assert.assertEquals(SignatureFormat.XADES_EPES_ENV_PESv2, SignatureFormat.fromString("XAdES/enveloped"));
        Assert.assertEquals(SignatureFormat.XADES_EPES_ENV_DIA, SignatureFormat.fromString("XAdES/DIA"));
        Assert.assertEquals(SignatureFormat.XADES_EPES_DET_1_1_1, SignatureFormat.fromString("XAdES/detached"));
        Assert.assertEquals(SignatureFormat.XADES_EPES_DET_1_3_2, SignatureFormat.fromString("XAdES132/detached"));
        Assert.assertEquals(SignatureFormat.XADES_T_EPES_ENV_1_3_2, SignatureFormat.fromString("XAdES-T/enveloped"));
        Assert.assertEquals(SignatureFormat.PKCS1_SHA256_RSA, SignatureFormat.fromString("PKCS#1/sha256"));
        Assert.assertEquals(SignatureFormat.PKCS1_SHA256_RSA, SignatureFormat.fromString("PKCS#1/sha256"));
    }

    @Test(expected = UnsupportedOperationException.class) public void fromString_UnknownSignatureFormat() throws Exception {
        SignatureFormat.fromString("plop");
        Assert.fail("An error should have been thrown on the previous line");
    }

}