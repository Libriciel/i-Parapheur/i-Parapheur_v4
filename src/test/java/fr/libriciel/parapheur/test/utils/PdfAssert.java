/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.test.utils;

import com.itextpdf.text.PageSize;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.util.PDFTextStripper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
public class PdfAssert {

    private static final int COLOR_DELTA = 1;


    // <editor-fold name="Private utils">

    private static @NotNull List<PDRectangle> getFormLocation(@NotNull PDDocument pdfDoc) throws IOException {

        List<PDRectangle> result = new ArrayList<>();
        PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();

        //noinspection unchecked
        List<PDPage> pages = docCatalog.getAllPages();
        for (PDPage page : pages) {
            for (PDAnnotation annotation : page.getAnnotations()) {
                result.add(annotation.getRectangle());
            }
        }

        return result;
    }

    private static @NotNull List<PDPixelMap> getImages(@NotNull PDDocument pdfDoc) throws IOException {

        List<PDPixelMap> result = new ArrayList<>();
        PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();

        //noinspection unchecked
        List<PDPage> pages = docCatalog.getAllPages();
        for (PDPage page : pages) {

            for (PDXObject xObject : page.getResources().getXObjects().values()) {
                if (xObject instanceof PDPixelMap) {
                    result.add((PDPixelMap) xObject);
                }
            }
        }

        return result;
    }

    /**
     * Checking pixel's color differences, between both images,
     * One pixel out of 100 is tested, allowing a small color difference.
     *
     * @param bufferedImageLeft  The expected one.
     * @param bufferedImageRight The actual one.
     * @param stampCoordinates   If set, we're asserting for visual differences inside the stamp location.
     */
    private static void assertImagesEquals(@NotNull BufferedImage bufferedImageLeft,
                                           @NotNull BufferedImage bufferedImageRight,
                                           @Nullable Rectangle stampCoordinates) {

        boolean hasDifferenceInStamp = false;

        for (int x = 0; x < bufferedImageLeft.getWidth(); x += 10) {
            for (int y = 0; y < bufferedImageLeft.getHeight(); y += 10) {

                int pixelBeforeRgb = bufferedImageLeft.getRGB(x, y);
                int pxBeforeR = (pixelBeforeRgb & 0x00ff0000) >> 16;
                int pxBeforeG = (pixelBeforeRgb & 0x0000ff00) >> 8;
                int pxBeforeB = pixelBeforeRgb & 0x000000ff;

                int pixelAfterRgb = bufferedImageRight.getRGB(x, y);
                int pxAfterR = (pixelAfterRgb & 0x00ff0000) >> 16;
                int pxAfterG = (pixelAfterRgb & 0x0000ff00) >> 8;
                int pxAfterB = pixelAfterRgb & 0x000000ff;

                if ((stampCoordinates != null) && stampCoordinates.contains(x, y)) {
                    hasDifferenceInStamp = hasDifferenceInStamp || (Math.abs(pxBeforeR - pxAfterR) > COLOR_DELTA);
                    hasDifferenceInStamp = hasDifferenceInStamp || (Math.abs(pxBeforeG - pxAfterG) > COLOR_DELTA);
                    hasDifferenceInStamp = hasDifferenceInStamp || (Math.abs(pxBeforeB - pxAfterB) > COLOR_DELTA);
                } else {
                    Assert.assertEquals(pxBeforeR, pxAfterR, COLOR_DELTA);
                    Assert.assertEquals(pxBeforeG, pxAfterG, COLOR_DELTA);
                    Assert.assertEquals(pxBeforeB, pxAfterB, COLOR_DELTA);
                }
            }
        }

        if (stampCoordinates != null) {
            Assert.assertTrue(hasDifferenceInStamp);
        }
    }

    private static @NotNull Rectangle pdfToPixelCoordinates(@NotNull Rectangle pdfCoordinates,
                                                            int imageHeight,
                                                            int pdfHeight) {

        // Changing scale

        float ratio = (float) imageHeight / pdfHeight;
        Rectangle result = new Rectangle(
                (int) Math.round(pdfCoordinates.getX() * ratio),
                (int) Math.round(pdfCoordinates.getY() * ratio),
                (int) Math.round(pdfCoordinates.getWidth() * ratio),
                (int) Math.round(pdfCoordinates.getHeight() * ratio)
        );

        // Reversing Y axis
        result.y = (int) (imageHeight - result.getHeight() - result.getY());

        return result;
    }

    // </editor-fold name="Private utils">

    public static void assertIsReadable(@Nullable String absolutePath) throws IOException {

        Assert.assertNotNull(absolutePath);

        PDDocument pdDoc = PDDocument.load(absolutePath);
        String defaultText = new PDFTextStripper().getText(pdDoc);

        Assert.assertNotNull(defaultText);
        Assert.assertTrue(defaultText.length() > 0);

        pdDoc.close();
    }

    public static void assertSignatureCountEquals(@Nullable String absolutePath,
                                                  int signatureCount) throws IOException {

        Assert.assertNotNull(absolutePath);

        PDDocument defaultDoc = PDDocument.load(absolutePath);
        Assert.assertTrue(defaultDoc.getSignatureFields().size() == signatureCount);

        defaultDoc.close();
    }

    public static void assertSignatureLocation(@Nullable String absolutePath,
                                               @Nullable Rectangle expected) throws IOException {

        Assert.assertNotNull(absolutePath);
        Assert.assertNotNull(expected);

        PDDocument doc = PDDocument.load(absolutePath);
        PDRectangle signRect = getFormLocation(doc).get(0);

        Assert.assertEquals(expected.getX(), signRect.getLowerLeftX(), 1F);
        Assert.assertEquals(expected.getY(), signRect.getLowerLeftY(), 1F);
        Assert.assertEquals(expected.getX() + expected.getWidth(), signRect.getUpperRightX(), 1F);
        Assert.assertEquals(expected.getY() + expected.getHeight(), signRect.getUpperRightY(), 1F);

        doc.close();
    }

    public static void assertStamped(@Nullable String originalAbsolutePath,
                                     @Nullable String stampedAbsolutePath) throws IOException {

        Assert.assertNotNull(originalAbsolutePath);
        Assert.assertNotNull(stampedAbsolutePath);

        PDDocument originalDoc = PDDocument.load(originalAbsolutePath);
        List<PDPixelMap> originalImages = getImages(originalDoc);
        Assert.assertEquals(0, originalImages.size());
        originalDoc.close();

        PDDocument stampedDoc = PDDocument.load(stampedAbsolutePath);
        List<PDPixelMap> stampedImages = getImages(stampedDoc);

        Assert.assertTrue(stampedImages.size() > 0); // FIXME : Why is there 2 images ?

        stampedDoc.close();
    }

    public static void assertStampLocation(@Nullable InputStream originalImageStream,
                                           @Nullable InputStream stampedImageStream,
                                           @Nullable Rectangle expectedLocation) throws IOException {

        Assert.assertNotNull(originalImageStream);
        Assert.assertNotNull(stampedImageStream);
        Assert.assertNotNull(expectedLocation);

        BufferedImage originalImage = ImageIO.read(originalImageStream);
        BufferedImage stampedImage = ImageIO.read(stampedImageStream);

        int pageA4Height = Math.round(PageSize.A4.getHeight());
        Rectangle stampPixLocation = pdfToPixelCoordinates(expectedLocation, stampedImage.getHeight(), pageA4Height);

        // Tests

        Assert.assertEquals(stampedImage.getWidth(), originalImage.getWidth());
        Assert.assertEquals(stampedImage.getHeight(), originalImage.getHeight());

        assertImagesEquals(originalImage, stampedImage, stampPixLocation);

        // Cleanup

        stampedImageStream.close();
        originalImageStream.close();
    }

}
