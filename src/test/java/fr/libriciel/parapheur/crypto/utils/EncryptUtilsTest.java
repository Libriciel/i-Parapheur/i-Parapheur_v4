/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.libriciel.parapheur.crypto.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class EncryptUtilsTest {

    @Test public void encryptDecrypt() {

        // Should encrypt and decrypt string with secret key
        String privateKey = "0123456789";

        // Simple and easy test

        String value = "simpletest";
        String encryptedValue = EncryptUtils.encrypt(value, privateKey);
        Assert.assertNotNull(encryptedValue);
        String decryptedValue = EncryptUtils.decrypt(encryptedValue, privateKey);
        Assert.assertEquals(value, decryptedValue);

        // With long value

        char[] charArray = new char[1024];
        Arrays.fill(charArray, '0');
        value = new String(charArray);

        encryptedValue = EncryptUtils.encrypt(value, privateKey);
        Assert.assertNotNull(encryptedValue);
        decryptedValue = EncryptUtils.decrypt(encryptedValue, privateKey);
        Assert.assertEquals(value, decryptedValue);

        // With long key

        privateKey = new String(charArray);
        value = "simpletest";
        encryptedValue = EncryptUtils.encrypt(value, privateKey);
        Assert.assertNotNull(encryptedValue);
        decryptedValue = EncryptUtils.decrypt(encryptedValue, privateKey);
        Assert.assertEquals(value, decryptedValue);
    }

    @Test public void encrypt_error() {

        String privateKey = "0123456789";

        Level previousLevel = Logger.getRootLogger().getLevel();
        Logger.getRootLogger().setLevel(Level.OFF);

        String errorResult = EncryptUtils.decrypt(",,,", privateKey);
        Assert.assertNull(errorResult);

        Logger.getRootLogger().setLevel(previousLevel);
    }

    @Test public void decrypt_error() throws UnsupportedEncodingException {

        Level previousLevel = Logger.getRootLogger().getLevel();
        Logger.getRootLogger().setLevel(Level.OFF);

        String errorEncrypt = EncryptUtils.encrypt("", "", "bad_Cypher_to_make_a_crash");
        Assert.assertNull(errorEncrypt);

        Logger.getRootLogger().setLevel(previousLevel);
    }
}