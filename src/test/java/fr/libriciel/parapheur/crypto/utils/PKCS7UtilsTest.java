/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.crypto.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.*;

public class PKCS7UtilsTest {

    private static final String TEST_FOLDER = "p7s/";
    private ClassLoader classLoader = getClass().getClassLoader();

    // <enum-fold name="Private utils">

    private @NotNull byte[] getBytesFromResource(@NotNull String name) throws Exception {

        InputStream inputStream = classLoader.getResourceAsStream(name);
        byte[] bytes = IOUtils.toByteArray(inputStream);
        inputStream.close();

        return bytes;
    }

    private @Nullable X509CertificateHolder getCertificateHolderFromResource(@NotNull String pdfName,
                                                                             @NotNull String sigName) throws Exception {

        byte[] pdf = getBytesFromResource(TEST_FOLDER + pdfName);
        byte[] sigPem = getBytesFromResource(TEST_FOLDER + sigName);
        byte[] sigDer = PKCS7Utils.pem2der(sigPem, PKCS7Utils.PEM_HEADER.getBytes(), PKCS7Utils.PEM_FOOTER.getBytes());

        Assert.assertNotNull(pdf);
        Assert.assertNotNull(sigDer);

        Security.addProvider(new BouncyCastleProvider());
        CMSSignedData signed = new CMSSignedData(new CMSProcessableByteArray(pdf), sigDer);
        return PKCS7Utils.getSignatureCertificateHolder(signed);
    }

    // <enum-fold name="Private utils">

    @Test public void indexOf_byteArray() throws Exception {

        byte[] outer = {1, 2, 3, 4};

        Assert.assertEquals(0, PKCS7Utils.indexOf(outer, new byte[]{1, 2}, 0));
        Assert.assertEquals(1, PKCS7Utils.indexOf(outer, new byte[]{2, 3}, 0));
        Assert.assertEquals(2, PKCS7Utils.indexOf(outer, new byte[]{3, 4}, 0));
        Assert.assertEquals(0, PKCS7Utils.indexOf(outer, new byte[]{}, 0));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, new byte[]{4, 4}, 0));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, new byte[]{4, 5, 6, 7, 8}, 0));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(new byte[]{}, new byte[]{1}, 0));

        Assert.assertEquals(2, PKCS7Utils.indexOf(outer, new byte[]{3, 4}, 0));
        Assert.assertEquals(2, PKCS7Utils.indexOf(outer, new byte[]{3, 4}, 2));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, new byte[]{3, 4}, 3));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, new byte[]{3, 4}, 6));

        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, new byte[]{3, 4}, 3));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, new byte[]{4, 4}, 0));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, new byte[]{1, 2}, 1));
    }

    @Test public void indexOf_byte() throws Exception {

        byte[] outer = {1, 2, 3, 4};

        Assert.assertEquals(0, PKCS7Utils.indexOf(outer, (byte) 1, 0));
        Assert.assertEquals(1, PKCS7Utils.indexOf(outer, (byte) 2, 0));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, (byte) 2, 2));
        Assert.assertEquals(2, PKCS7Utils.indexOf(outer, (byte) 3, 0));

        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, (byte) 5, 0));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(outer, (byte) 1, 9));
        Assert.assertEquals(-1, PKCS7Utils.indexOf(new byte[]{}, (byte) 0, 0));
    }

    @Test public void verify() throws Exception {
        HashMap<String, String> pdfSignaturesMap = new HashMap<>();

        // Fetching PDF and P7S in /p7s/ folder

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        List<Resource> testPdfList = Arrays.asList(resolver.getResources(TEST_FOLDER + "*.pdf"));

        for (Resource testPdf : testPdfList) {
            Resource sigPdf = resolver.getResource(TEST_FOLDER + testPdf.getFilename() + ".p7s");

            if (sigPdf.exists()) {
                pdfSignaturesMap.put(TEST_FOLDER + testPdf.getFilename(), TEST_FOLDER + sigPdf.getFilename());
            } else {
                Assert.fail("Missing signature p7s file for " + testPdf.getFilename());
            }
        }

        // Checking valid signatures

        for (Map.Entry<String, String> pdfSignatureEntry : pdfSignaturesMap.entrySet()) {

            byte[] pdf = getBytesFromResource(pdfSignatureEntry.getKey());
            byte[] sigPem = getBytesFromResource(pdfSignatureEntry.getValue());
            byte[] sigDer = PKCS7Utils.pem2der(sigPem, PKCS7Utils.PEM_HEADER.getBytes(), PKCS7Utils.PEM_FOOTER.getBytes());

            Assert.assertNotNull(sigDer);
            Assert.assertTrue(PKCS7Utils.verify(pdf, sigDer));
        }

        // Checking invalid signature

        byte[] pdf = getBytesFromResource(TEST_FOLDER + "01.pdf");
        byte[] sigPem = getBytesFromResource(TEST_FOLDER + "02.pdf.p7s");
        byte[] sigDer = PKCS7Utils.pem2der(sigPem, PKCS7Utils.PEM_HEADER.getBytes(), PKCS7Utils.PEM_FOOTER.getBytes());

        Level previousLevel = Logger.getRootLogger().getLevel();
        Logger.getRootLogger().setLevel(Level.OFF);

        Assert.assertNotNull(sigDer);
        Assert.assertFalse(PKCS7Utils.verify(pdf, sigDer));

        Logger.getRootLogger().setLevel(previousLevel);
    }

    @Test public void verifyAndCount() throws Exception {

        URL pdfUri = classLoader.getResource(TEST_FOLDER + "01.pdf");
        Assert.assertNotNull(pdfUri);
        File pdfFile = new File(pdfUri.toURI());

        byte[] sigPem = getBytesFromResource(TEST_FOLDER + "01.pdf.p7s");
        byte[] sigDer = PKCS7Utils.pem2der(sigPem, PKCS7Utils.PEM_HEADER.getBytes(), PKCS7Utils.PEM_FOOTER.getBytes());

        Assert.assertNotNull(sigDer);
        Assert.assertTrue(PKCS7Utils.verify(pdfFile, sigDer));
    }

    @Test public void holderToCertificate_single() throws Exception {

        X509CertificateHolder holder = getCertificateHolderFromResource("01.pdf", "01.pdf.p7s");
        Assert.assertNotNull(holder);

        X509Certificate certif = PKCS7Utils.holderToCertificate(holder);
        Assert.assertNotNull(certif);
        Assert.assertEquals(1593797040000L, certif.getNotAfter().getTime());
        Assert.assertEquals(1291397040000L, certif.getNotBefore().getTime());
        Assert.assertTrue(StringUtils.contains(certif.getIssuerDN().getName(), "OU=ADULLACT-Projet"));
    }

    @Test public void holderToCertificate_list() throws Exception {

        List<X509CertificateHolder> holderList = new ArrayList<>();
        holderList.add(getCertificateHolderFromResource("01.pdf", "01.pdf.p7s"));
        holderList.add(getCertificateHolderFromResource("02.pdf", "02.pdf.p7s"));
        holderList.add(null);
        holderList.add(getCertificateHolderFromResource("03.pdf", "03.pdf.p7s"));

        JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
        converter.setProvider(new BouncyCastleProvider());
        List<X509Certificate> convertedList = PKCS7Utils.holderToCertificate(holderList, converter);

        Assert.assertEquals(3, convertedList.size());
        Assert.assertEquals(1593797040000L, convertedList.get(0).getNotAfter().getTime());
        Assert.assertEquals(1291397040000L, convertedList.get(0).getNotBefore().getTime());
        Assert.assertEquals(1593797040000L, convertedList.get(1).getNotAfter().getTime());
        Assert.assertEquals(1291397040000L, convertedList.get(1).getNotBefore().getTime());
        Assert.assertEquals(1593797040000L, convertedList.get(2).getNotAfter().getTime());
        Assert.assertEquals(1291397040000L, convertedList.get(2).getNotBefore().getTime());

        //

        List<X509Certificate> nullList = PKCS7Utils.holderToCertificate(null, converter);
        Assert.assertNotNull(nullList);
        Assert.assertEquals(0, nullList.size());

        //

        ArrayList<X509CertificateHolder> emptyInput = new ArrayList<>();
        List<X509Certificate> emptyList = PKCS7Utils.holderToCertificate(emptyInput, converter);
        Assert.assertNotNull(emptyList);
        Assert.assertEquals(0, emptyList.size());
    }

}