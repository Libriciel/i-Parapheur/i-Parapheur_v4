package fr.libriciel.parapheur.crypto.utils;

import fr.libriciel.parapheur.models.DigestAlgorithm;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.net.URL;


public class SignatureUtilsTest {

    private ClassLoader classLoader = getClass().getClassLoader();

    // <enum-fold name="Private utils">

    private @NotNull byte[] getBytesFromResource(@NotNull String name) throws Exception {

        InputStream inputStream = classLoader.getResourceAsStream(name);
        byte[] bytes = IOUtils.toByteArray(inputStream);
        inputStream.close();

        return bytes;
    }

    // </enum-fold name="Private utils">

    @Test public void getFileDigest() throws Exception {

        URL pdf01URL = classLoader.getResource("p7s/01.pdf");
        Assert.assertNotNull(pdf01URL);
        File pdf01File = new File(pdf01URL.getFile());

        byte[] sha1Hash = SignatureUtils.getFileDigest(pdf01File, DigestAlgorithm.MD5, 2048);
        byte[] md5Hash = SignatureUtils.getFileDigest(pdf01File, DigestAlgorithm.SHA1, 2048);
        byte[] sha256Hash = SignatureUtils.getFileDigest(pdf01File, DigestAlgorithm.SHA256, 2048);

        Assert.assertNotNull(sha1Hash);
        Assert.assertNotNull(md5Hash);
        Assert.assertNotNull(sha256Hash);

//        System.out.println(Base64.byteArrayToBase64(sha1Hash, 0, sha1Hash.length));
//        System.out.println(Base64.byteArrayToBase64(md5Hash, 0, md5Hash.length));
//        System.out.println(Base64.byteArrayToBase64(sha256Hash, 0, sha256Hash.length));
    }

    @Test public void getBytesDigest() throws Exception {

    }
}