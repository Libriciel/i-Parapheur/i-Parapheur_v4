/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.pdf.utils;

import fr.libriciel.parapheur.test.utils.PdfAssert;
import org.alfresco.util.TempFileProvider;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;


public class PdfSealUtilsTest {

    private ClassLoader classLoader = getClass().getClassLoader();


    @Test public void signPdf() throws Exception {

        URL defaultUrl = classLoader.getResource("pdf/default.pdf");
        URL p12Url = classLoader.getResource("p12/test.p12");
        URL pdfFolderUrl = classLoader.getResource("pdf/");

        Assert.assertNotNull(pdfFolderUrl);
        Assert.assertNotNull(defaultUrl);
        Assert.assertNotNull(p12Url);

        String defaultPath = defaultUrl.getPath();
        String resultPath = TempFileProvider.createTempFile("sealed", "pdf").getAbsolutePath();

        // Signature

        PdfSealUtils.signPdf(
                defaultPath,
                resultPath,
                p12Url.getPath(),
                "test",
                "test",
                false,
                false
        );

        // Check

        PdfAssert.assertSignatureCountEquals(resultPath, 1);
    }

}