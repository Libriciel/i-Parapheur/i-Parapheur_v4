/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.pdf.utils;

import com.itextpdf.text.pdf.parser.LineSegment;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.parser.Vector;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class SignatureTagLocatorTextRenderListenerTest {

    @Test public void renderText() throws Exception {

        List<Vector> outResult = new ArrayList<>();
        SignatureTagLocatorTextRenderListener locatorTextRenderListener = new SignatureTagLocatorTextRenderListener("#test#", outResult);

        TextRenderInfo textRenderInfo = mock(TextRenderInfo.class);
        when(textRenderInfo.getAscentLine()).thenReturn(new LineSegment(new Vector(1, 1, 1), new Vector(8, 8, 8)));
        when(textRenderInfo.getBaseline()).thenReturn(new LineSegment(new Vector(2, 2, 2), new Vector(9, 9, 9)));

        // Exact match

        outResult.clear();

        when(textRenderInfo.getText()).thenReturn("#test#");
        locatorTextRenderListener.renderText(textRenderInfo);
        Assert.assertEquals(1, outResult.size());

        // No match

        outResult.clear();

        when(textRenderInfo.getText()).thenReturn("nope nope");
        locatorTextRenderListener.renderText(textRenderInfo);
        Assert.assertEquals(0, outResult.size());

//        // Match
//
//        outResult.clear();
//
//        when(textRenderInfo.getText()).thenReturn("__#test#__");
//        locatorTextRenderListener.renderText(textRenderInfo);
//        Assert.assertEquals(1, outResult.size());

        // Split match

        outResult.clear();

        when(textRenderInfo.getText()).thenReturn("__#te");
        locatorTextRenderListener.renderText(textRenderInfo);
        Assert.assertEquals(0, outResult.size());

        when(textRenderInfo.getText()).thenReturn("st#__");
        locatorTextRenderListener.renderText(textRenderInfo);
        Assert.assertEquals(1, outResult.size());

        // Split no (but almost) match

        outResult.clear();

        when(textRenderInfo.getText()).thenReturn("_#te");
        locatorTextRenderListener.renderText(textRenderInfo);
        Assert.assertEquals(0, outResult.size());

        when(textRenderInfo.getText()).thenReturn("s");
        locatorTextRenderListener.renderText(textRenderInfo);
        Assert.assertEquals(0, outResult.size());
    }

}