/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.pdf.utils;


import com.itextpdf.text.pdf.PdfReader;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.io.InputStream;
import java.util.Map;

public class PdfUtilsTest {

    private ClassLoader classLoader = getClass().getClassLoader();


    @Test public void retrieveSignatureTagPosition() throws Exception {

        InputStream defaultStream = classLoader.getResourceAsStream("pdf/default.pdf");
        PdfReader defaultReader = new PdfReader(defaultStream);
        Map.Entry<Integer, Rectangle> defaultPosition = PdfUtils.findSignatureTagPosition(
                defaultReader,
                PdfUtils.DEFAULT_SIGNATURE_TAG,
                PdfUtils.DEFAULT_SIGNATURE_SIZE);
        defaultReader.close();
        defaultStream.close();

        Assert.assertNull(defaultPosition);

        //

        InputStream defaultTagStream = classLoader.getResourceAsStream("pdf/default_tag.pdf");
        PdfReader defaultTagReader = new PdfReader(defaultTagStream);
        Map.Entry<Integer, Rectangle> defaultTagPosition = PdfUtils.findSignatureTagPosition(
                defaultTagReader,
                PdfUtils.DEFAULT_SIGNATURE_TAG,
                new Point(200, 150));
        defaultTagReader.close();
        defaultTagStream.close();

        Assert.assertNotNull(defaultTagPosition);
        Assert.assertEquals(new Integer(1), defaultTagPosition.getKey());
        Assert.assertEquals(148, defaultTagPosition.getValue().x, 1);
        Assert.assertEquals(446, defaultTagPosition.getValue().y, 1);
        Assert.assertEquals(200, defaultTagPosition.getValue().width, 1);
        Assert.assertEquals(150, defaultTagPosition.getValue().height, 1);

        //

        InputStream tagPositionBigStream = classLoader.getResourceAsStream("pdf/tag_position_big_p31378_x248_y66.pdf");
        PdfReader tagPositionBigReader = new PdfReader(tagPositionBigStream);
        Map.Entry<Integer, Rectangle> tagPositionBigPosition = PdfUtils.findSignatureTagPosition(
                tagPositionBigReader,
                PdfUtils.DEFAULT_SIGNATURE_TAG,
                PdfUtils.DEFAULT_SIGNATURE_SIZE);
        tagPositionBigReader.close();
        tagPositionBigStream.close();

        Assert.assertNotNull(tagPositionBigPosition);
        Assert.assertEquals(new Integer(31378), tagPositionBigPosition.getKey());
        Assert.assertEquals(248, tagPositionBigPosition.getValue().x, 1);
        Assert.assertEquals(66, tagPositionBigPosition.getValue().y, 1);
        Assert.assertEquals(100, tagPositionBigPosition.getValue().width, 1);
        Assert.assertEquals(100, tagPositionBigPosition.getValue().width, 1);

        //

        InputStream tagPosition01Stream = classLoader.getResourceAsStream("pdf/tag_position_01_p1_x136_y186.pdf");
        PdfReader tagPosition01Reader = new PdfReader(tagPosition01Stream);
        Map.Entry<Integer, Rectangle> tagPosition01Position = PdfUtils.findSignatureTagPosition(
                tagPosition01Reader,
                PdfUtils.DEFAULT_SIGNATURE_TAG,
                PdfUtils.DEFAULT_SIGNATURE_SIZE);
        tagPosition01Reader.close();
        tagPosition01Stream.close();

        Assert.assertNotNull(tagPosition01Position);
        Assert.assertEquals(new Integer(1), tagPosition01Position.getKey());
        Assert.assertEquals(136, tagPosition01Position.getValue().x, 1);
        Assert.assertEquals(186, tagPosition01Position.getValue().y, 1);
        Assert.assertEquals(100, tagPosition01Position.getValue().width, 1);
        Assert.assertEquals(100, tagPosition01Position.getValue().height, 1);
    }

}