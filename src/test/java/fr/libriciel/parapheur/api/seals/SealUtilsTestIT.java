/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.libriciel.parapheur.api.seals;

import fr.libriciel.parapheur.api.seals.exceptions.NodeRefIsNotSealException;
import fr.libriciel.parapheur.crypto.utils.EncryptUtils;
import fr.libriciel.parapheur.test.utils.ClassLoaderUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.rad.test.AbstractAlfrescoIT;
import org.alfresco.rad.test.AlfrescoTestRunner;
import org.alfresco.rad.test.Remote;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(AlfrescoTestRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SealUtilsTestIT extends AbstractAlfrescoIT {

    private ClassLoader classLoader = getClass().getClassLoader();

    private static void cleanupCertificateList(@NotNull ServiceRegistry serviceRegistry) {

        List<SealCertificate> oldSealCertifList = SealUtils.listSealCertificates(serviceRegistry);
        for (SealCertificate oldSealCertif : oldSealCertifList) {
            SealUtils.deleteSealCertificate(oldSealCertif.getId(), serviceRegistry);
        }

        Assert.assertEquals(0, SealUtils.listSealCertificates(serviceRegistry).size());
    }

    // <editor-fold desc="Private utils">

    /**
     * We don't want to use the updateSealCertificate result, on tests.
     * We want a full "hard" refresh, fetched.
     */
    private static @Nullable SealCertificate fetchSealCertificate(@NotNull final String sealCertificateId,
                                                                  @NotNull ServiceRegistry serviceRegistry) {

        Predicate isTheUpdatedOnePredicate = new Predicate() {
            @Override public boolean evaluate(Object object) {
                return StringUtils.equals(((SealCertificate) object).getId(), sealCertificateId);
            }
        };

        List<SealCertificate> listAfter = SealUtils.listSealCertificates(serviceRegistry);
        CollectionUtils.filter(listAfter, isTheUpdatedOnePredicate);

        // Send result

        Assert.assertFalse(listAfter.size() > 1);
        return listAfter.isEmpty() ? null : listAfter.get(0);
    }

    @Before public void setUp() throws Exception {
        AuthenticationUtil.setRunAsUserSystem();
    }

    private @NotNull SealCertificate setupUpdateSealCertificate() throws Exception {

        String p12Base64 = ClassLoaderUtils.getInBase64(classLoader, "p12/test.p12");
        SealCertificate.CachetDescription description = new SealCertificate.CachetDescription();
        SealCertificate rootSeal = new SealCertificate("Certif 01", p12Base64, "test", description);

        cleanupCertificateList(getServiceRegistry());

        rootSeal = SealUtils.createSealCertificate(rootSeal, "test", getServiceRegistry());
        Assert.assertNotNull(rootSeal);

        return rootSeal;
    }

    // </editor-fold desc="Private utils">

    @Test public void isComplete() throws Exception {

        SealCertificate.CachetDescription description = new SealCertificate.CachetDescription();

        Assert.assertTrue(SealUtils.isComplete(new SealCertificate("", "", "", description)));
        Assert.assertTrue(SealUtils.isComplete(new SealCertificate("titre", "AAAA", "password", description)));

        Assert.assertFalse(SealUtils.isComplete(new SealCertificate(null, "AAAA", "password", description)));
        Assert.assertFalse(SealUtils.isComplete(new SealCertificate("titre", null, "password", description)));
        Assert.assertFalse(SealUtils.isComplete(new SealCertificate("titre", "AAAA", null, description)));
        Assert.assertFalse(SealUtils.isComplete(new SealCertificate("titre", "AAAA", "password", null)));
    }

    @Test public void getCertificatesNodeRef() throws Exception {

        // Cleanup, if the node already exists

        NodeRef oldNode = SealUtils.getCertificatesNodeRef(getServiceRegistry());
        getServiceRegistry().getNodeService().deleteNode(oldNode);

        // Tests

        NodeRef firstGet = SealUtils.getCertificatesNodeRef(getServiceRegistry());
        NodeRef secondGet = SealUtils.getCertificatesNodeRef(getServiceRegistry());

        Assert.assertNotNull(firstGet);
        Assert.assertNotNull(secondGet);
        Assert.assertEquals(firstGet, secondGet);
    }

    @Test public void getBase64Content_ErrorReadingContent() {

        ContentReader mockedReader01 = mock(ContentReader.class);
        when(mockedReader01.getContentInputStream()).thenThrow(new ContentIOException("Mocked ContentIOException exception"));
        Assert.assertNull(SealUtils.getBase64Content(mockedReader01));

        ContentReader mockedReader02 = mock(ContentReader.class);
        when(mockedReader02.getContentInputStream()).thenThrow(new ContentIOException("Mocked IOException exception"));
        Assert.assertNull(SealUtils.getBase64Content(mockedReader02));
    }

    @Test public void createListDeleteSealCertificate() throws Exception {

        String p12Base64 = ClassLoaderUtils.getInBase64(classLoader, "p12/test.p12");
        SealCertificate.CachetDescription description = new SealCertificate.CachetDescription();

        SealCertificate sealCertificate01 = new SealCertificate("Certif 01", p12Base64, "test", description);
        SealCertificate sealCertificate02 = new SealCertificate("Certif 02", p12Base64, "test", description);
        SealCertificate sealCertificate03 = new SealCertificate("Certif 03", p12Base64, "test", description);

        String logoBase64 = ClassLoaderUtils.getInBase64(classLoader, "pdf/logo.png");
        sealCertificate02.setImage(logoBase64);

        // Tests

        cleanupCertificateList(getServiceRegistry());

        // Create

        SealUtils.createSealCertificate(sealCertificate01, "test", getServiceRegistry());
        Assert.assertEquals(1, SealUtils.listSealCertificates(getServiceRegistry()).size());
        SealUtils.createSealCertificate(sealCertificate02, "test", getServiceRegistry());
        Assert.assertEquals(2, SealUtils.listSealCertificates(getServiceRegistry()).size());
        SealUtils.createSealCertificate(sealCertificate01, "test", getServiceRegistry());
        Assert.assertEquals(2, SealUtils.listSealCertificates(getServiceRegistry()).size());
        SealUtils.createSealCertificate(sealCertificate03, "test", getServiceRegistry());
        Assert.assertEquals(3, SealUtils.listSealCertificates(getServiceRegistry()).size());

        // Delete

        SealUtils.deleteSealCertificate(sealCertificate01.getId(), getServiceRegistry());
        Assert.assertEquals(2, SealUtils.listSealCertificates(getServiceRegistry()).size());
        SealUtils.deleteSealCertificate(sealCertificate02.getId(), getServiceRegistry());
        Assert.assertEquals(1, SealUtils.listSealCertificates(getServiceRegistry()).size());
        SealUtils.deleteSealCertificate(sealCertificate03.getId(), getServiceRegistry());
        Assert.assertEquals(0, SealUtils.listSealCertificates(getServiceRegistry()).size());
    }

    @Test(expected = MalformedNodeRefException.class) public void deleteSealCertificate_InvalidId() throws Exception {

        // Should throw the exception
        SealUtils.deleteSealCertificate("bad_id", getServiceRegistry());
        Assert.fail("An error should have been thrown on the previous line");
    }

    @Test(expected = InvalidNodeRefException.class) public void deleteSealCertificate_UnknownNode() throws Exception {

        // Should throw the exception
        SealUtils.deleteSealCertificate("00000000-0000-0000-0000-000000000000", getServiceRegistry());
        Assert.fail("An error should have been thrown on the previous line");
    }

    @Test(expected = NodeRefIsNotSealException.class) public void deleteSealCertificate_IsNotSeal() throws Exception {

        // Should throw the exception
        NodeRef sealDir = SealUtils.getCertificatesNodeRef(getServiceRegistry());
        SealUtils.deleteSealCertificate(sealDir.getId(), getServiceRegistry());
        Assert.fail("An error should have been thrown on the previous line");
    }

    @Test(expected = IllegalArgumentException.class) public void createSealCertificate_IncompleteCertificate() throws Exception {

        SealCertificate sealCertificate = new SealCertificate("Certif 01", null, null, null);
        SealUtils.createSealCertificate(sealCertificate, "test", getServiceRegistry());
        Assert.fail("An error should have been thrown on the previous line");
    }

    @Test public void updateSealCertificate_UpdatePassword() throws Exception {

        SealCertificate rootSeal = setupUpdateSealCertificate();

        // Test model (the password is never deserialized)

        SealCertificate tempSeal = new SealCertificate();
        tempSeal.setId(rootSeal.getId());
        tempSeal.setPassword("password_update");

        SealUtils.updateSealCertificate(tempSeal, "test", getServiceRegistry());
        SealCertificate sealAfterUpdate = fetchSealCertificate(tempSeal.getId(), getServiceRegistry());

        Assert.assertNotNull(sealAfterUpdate);
        Assert.assertNull(sealAfterUpdate.getDeserializedPassword());

        // Test data (the password is still stored)

        NodeService nodeService = getServiceRegistry().getNodeService();
        NodeRef certsHome = SealUtils.getCertificatesNodeRef(getServiceRegistry());

        List<ChildAssociationRef> childrens = nodeService.getChildAssocs(certsHome);
        for (ChildAssociationRef child : childrens) {
            String childId = (String) nodeService.getProperty(child.getChildRef(), ContentModel.PROP_NAME);

            if (StringUtils.equals(childId, rootSeal.getId())) {
                String childEncryptedPassword = (String) nodeService.getProperty(child.getChildRef(), ContentModel.PROP_PASSWORD);
                String expectedEncryptedPassword = EncryptUtils.encrypt("password_update", "test");
                Assert.assertEquals(expectedEncryptedPassword, childEncryptedPassword);
            }
        }

        //

        cleanupCertificateList(getServiceRegistry());
    }

    @Test public void updateSealCertificate_UpdateOriginalNameAndDescription() throws Exception {

        SealCertificate rootSeal = setupUpdateSealCertificate();

        SealCertificate.CachetDescription descriptionUpdate = new SealCertificate.CachetDescription();
        descriptionUpdate.setAlias("alias_update");
        descriptionUpdate.setIssuerDN("issuer_update");

        // Test

        SealCertificate tempSeal = new SealCertificate();
        tempSeal.setId(rootSeal.getId());
        tempSeal.setDescription(descriptionUpdate);
        tempSeal.setOriginalName("original_name_update");

        SealUtils.updateSealCertificate(tempSeal, "test", getServiceRegistry());
        SealCertificate sealAfterUpdate = fetchSealCertificate(rootSeal.getId(), getServiceRegistry());

        Assert.assertNotNull(sealAfterUpdate);
        Assert.assertEquals("original_name_update", sealAfterUpdate.getOriginalName());
        Assert.assertEquals("alias_update", sealAfterUpdate.getDescription().getAlias());
        Assert.assertEquals("issuer_update", sealAfterUpdate.getDescription().getIssuerDN());

        //

        cleanupCertificateList(getServiceRegistry());
    }

    @Test public void updateSealCertificate_UpdateCertificate() throws Exception {

        SealCertificate rootSeal = setupUpdateSealCertificate();
        String p12Base64 = ClassLoaderUtils.getInBase64(classLoader, "p12/webservice.p12");

        // Test

        SealCertificate tempSeal = new SealCertificate();
        tempSeal.setId(rootSeal.getId());
        tempSeal.setCertificate(p12Base64);

        SealUtils.updateSealCertificate(tempSeal, "test", getServiceRegistry());
        SealCertificate sealAfterUpdate = fetchSealCertificate(rootSeal.getId(), getServiceRegistry());

        Assert.assertNotNull(sealAfterUpdate);
        Assert.assertNull(sealAfterUpdate.getDescription());
        Assert.assertNotEquals(sealAfterUpdate.getCertificate(), rootSeal.getCertificate());

        //

        cleanupCertificateList(getServiceRegistry());
    }

    @Test public void updateSealCertificate_UpdateImage() throws Exception {

        SealCertificate rootSeal = setupUpdateSealCertificate();
        String logoBase64 = ClassLoaderUtils.getInBase64(classLoader, "pdf/logo.png");

        // Test

        SealCertificate tempSeal = new SealCertificate();
        tempSeal.setId(rootSeal.getId());
        tempSeal.setImage(logoBase64);

        SealUtils.updateSealCertificate(tempSeal, "test", getServiceRegistry());
        SealCertificate sealAfterUpdate = fetchSealCertificate(rootSeal.getId(), getServiceRegistry());

        Assert.assertNotNull(sealAfterUpdate);
        Assert.assertNull(rootSeal.getImage());
        Assert.assertNotNull(sealAfterUpdate.getImage());
        Assert.assertEquals(logoBase64, sealAfterUpdate.getImage());

        //

        cleanupCertificateList(getServiceRegistry());
    }

    @Test public void getSetMailForWarnMail() throws Exception {

        String mailModel = "test_mail_" + System.currentTimeMillis();

        String before = SealUtils.getMailForWarnMail(getServiceRegistry());
        SealUtils.setMailForWarnMail(mailModel, getServiceRegistry());
        String after = SealUtils.getMailForWarnMail(getServiceRegistry());

        Assert.assertNotEquals(before, after);
        Assert.assertEquals(mailModel, after);
    }
}