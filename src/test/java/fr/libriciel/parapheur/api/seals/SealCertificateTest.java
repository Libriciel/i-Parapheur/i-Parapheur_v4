package fr.libriciel.parapheur.api.seals;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.junit.Test;

public class SealCertificateTest {

    @Test public void serialization_Full() {

        // Setup

        SealCertificate beforeSeal = new SealCertificate();
        beforeSeal.setId("id_test");
        beforeSeal.setTitle("title_test");
        beforeSeal.setOriginalName("original_test");
        beforeSeal.setCertificate("certificate_test");
        beforeSeal.setPassword("password_test");
        beforeSeal.setImage("image_test");

        SealCertificate.CachetDescription cachetDescription = new SealCertificate.CachetDescription(null, null, -1, null);
        cachetDescription.setAlias("alias_test");
        cachetDescription.setIssuerDN("issuer_test");
        cachetDescription.setNotAfter(999);
        cachetDescription.setSubjectDN("subject_test");

        beforeSeal.setDescription(cachetDescription);

        // Test

        Gson gson = new GsonBuilder().create();
        String serialized = gson.toJson(beforeSeal);
        SealCertificate afterSeal = gson.fromJson(serialized, SealCertificate.class);

        Assert.assertNotNull(afterSeal);
        Assert.assertEquals(beforeSeal.getId(), afterSeal.getId());
        Assert.assertEquals(beforeSeal.getTitle(), afterSeal.getTitle());
        Assert.assertEquals(beforeSeal.getOriginalName(), afterSeal.getOriginalName());
        Assert.assertEquals(beforeSeal.getCertificate(), afterSeal.getCertificate());
        Assert.assertEquals(beforeSeal.getDeserializedPassword(), afterSeal.getDeserializedPassword());
        Assert.assertEquals(beforeSeal.getImage(), afterSeal.getImage());
        Assert.assertEquals(beforeSeal.getDescription().getAlias(), afterSeal.getDescription().getAlias());
        Assert.assertEquals(beforeSeal.getDescription().getIssuerDN(), afterSeal.getDescription().getIssuerDN());
        Assert.assertEquals(beforeSeal.getDescription().getNotAfter(), afterSeal.getDescription().getNotAfter());
        Assert.assertEquals(beforeSeal.getDescription().getSubjectDN(), afterSeal.getDescription().getSubjectDN());
        Assert.assertEquals(beforeSeal.toString(), afterSeal.toString());
    }

    @Test public void serialization_Empty() {
        Gson gson = new GsonBuilder().create();

        SealCertificate beforeSeal = new SealCertificate();
        String serialized = gson.toJson(beforeSeal);
        SealCertificate afterSeal = gson.fromJson(serialized, SealCertificate.class);

        Assert.assertNotNull(afterSeal);
        Assert.assertNull(afterSeal.getId());
        Assert.assertNull(afterSeal.getTitle());
        Assert.assertEquals(beforeSeal.toString(), afterSeal.toString());
    }
}