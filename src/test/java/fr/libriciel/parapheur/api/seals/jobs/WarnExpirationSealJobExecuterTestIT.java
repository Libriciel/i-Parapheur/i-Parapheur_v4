/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.libriciel.parapheur.api.seals.jobs;

import fr.libriciel.parapheur.api.seals.SealCertificate;
import fr.libriciel.parapheur.api.seals.SealUtils;
import fr.libriciel.parapheur.templates.TemplateUtils;
import fr.libriciel.parapheur.test.utils.ClassLoaderUtils;
import org.alfresco.rad.test.AbstractAlfrescoIT;
import org.alfresco.rad.test.AlfrescoTestRunner;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.action.ActionServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(value = AlfrescoTestRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WarnExpirationSealJobExecuterTestIT extends AbstractAlfrescoIT {

    private ClassLoader classLoader = getClass().getClassLoader();

    @Before public void setupExistingSeals() throws Exception {

        AuthenticationUtil.setRunAsUserSystem();
        Calendar cal = Calendar.getInstance();

        String p12Base64 = ClassLoaderUtils.getInBase64(classLoader, "p12/test.p12");
        SealCertificate.CachetDescription description = new SealCertificate.CachetDescription();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 30);
        description.setNotAfter(cal.getTime().getTime());
        description.setIssuerDN("DN: Libriciel");

        // Create seal before

        UserTransaction transaction = getServiceRegistry().getTransactionService().getUserTransaction();
        transaction.begin();

        SealCertificate sealCertificat30days = new SealCertificate("Certif 30 days", p12Base64, "test", description);
        SealUtils.createSealCertificate(sealCertificat30days, "test", getServiceRegistry());

        cal.setTime(new Date());
        cal.add(Calendar.DATE, -30);
        description.setNotAfter(cal.getTime().getTime());
        SealCertificate sealCertificateMinus30days = new SealCertificate("Certif minus 30 days", p12Base64, "test", description);
        SealUtils.createSealCertificate(sealCertificateMinus30days, "test", getServiceRegistry());

        transaction.commit();
    }

    @Test public void createEmailTemplateModel_EmptyList() throws Exception {

        WarnExpirationSealJobExecuter jobExecuter = new WarnExpirationSealJobExecuter();
        jobExecuter.setServiceRegistry(getServiceRegistry());

        Map<String, Object> templateModel = jobExecuter.createEmailTemplateModel(new ArrayList<SealCertificate>());
        Assert.assertNull(templateModel);
    }

    @Test public void buildModelForMail() throws Exception {

        // Create job executer

        WarnExpirationSealJobExecuter jobExecuter = new WarnExpirationSealJobExecuter();
        jobExecuter.setServiceRegistry(getServiceRegistry());

        List<SealCertificate> seals = SealUtils.listSealCertificates(getServiceRegistry());

        // Expect model to be empty
        jobExecuter.setDaysUntilExpiration(-1); // -1 = none
        Map<String, Object> model = jobExecuter.createEmailTemplateModel(seals);
        Assert.assertNull(model);

        // Expect model to have -30 cert
        jobExecuter.setDaysUntilExpiration(0); // 0 = expired only
        model = jobExecuter.createEmailTemplateModel(seals);
        Assert.assertNotNull(model);
        List listSeals = (List) model.get("seals");
        Assert.assertEquals(1, listSeals.size());

        // Expect model to have -30 certs
        jobExecuter.setDaysUntilExpiration(25);
        model = jobExecuter.createEmailTemplateModel(seals);
        Assert.assertNotNull(model);
        listSeals = (List) model.get("seals");
        Assert.assertEquals(1, listSeals.size());

        // Expect model to have -30 and 30 certs
        jobExecuter.setDaysUntilExpiration(31);
        model = jobExecuter.createEmailTemplateModel(seals);
        Assert.assertNotNull(model);
        listSeals = (List) model.get("seals");
        Assert.assertEquals(2, listSeals.size());

        jobExecuter.execute();
    }

    @Test(expected = ActionServiceException.class) public void buildMail_CannotCreateAction() throws Exception {

        // We're mocking a regular serviceRegistry,
        // Except for the fake actionService, that will return a null object

        ActionService actionServiceMock = mock(ActionService.class);
        when(actionServiceMock.createAction("", new HashMap<String, Serializable>())).thenReturn(null);

        ServiceRegistry serviceRegistryMock = mock(ServiceRegistry.class);
        when(serviceRegistryMock.getTemplateService()).thenReturn(getServiceRegistry().getTemplateService());
        when(serviceRegistryMock.getNodeService()).thenReturn(getServiceRegistry().getNodeService());
        when(serviceRegistryMock.getNamespaceService()).thenReturn(getServiceRegistry().getNamespaceService());
        when(serviceRegistryMock.getSearchService()).thenReturn(getServiceRegistry().getSearchService());
        when(serviceRegistryMock.getActionService()).thenReturn(actionServiceMock);

        // Test

        WarnExpirationSealJobExecuter jobExecuter = new WarnExpirationSealJobExecuter();
        jobExecuter.setServiceRegistry(serviceRegistryMock);

        List<SealCertificate> listSeals = SealUtils.listSealCertificates(getServiceRegistry());
        jobExecuter.setDaysUntilExpiration(0);
        Map<String, Object> model = jobExecuter.createEmailTemplateModel(listSeals);
        String templateStr = TemplateUtils.getTemplateFromName(WarnExpirationSealJobExecuter.TEMPLATE_NAME, getServiceRegistry());
        Assert.assertNotNull(model);

        jobExecuter.sendMail(model, templateStr, new ArrayList<String>());
        Assert.fail("An error should have been thrown on the previous line");
    }
}
