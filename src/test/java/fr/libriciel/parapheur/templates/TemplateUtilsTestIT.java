/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.libriciel.parapheur.templates;

import fr.libriciel.parapheur.test.utils.ClassLoaderUtils;
import org.alfresco.rad.test.AbstractAlfrescoIT;
import org.alfresco.rad.test.AlfrescoTestRunner;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.apache.commons.codec.binary.Base64;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Créé par lhameury le 5/11/17.
 */
@RunWith(value = AlfrescoTestRunner.class)
public class TemplateUtilsTestIT extends AbstractAlfrescoIT {

    private ClassLoader classLoader = getClass().getClassLoader();

    private static final String TEST_MODEL_NAME = "test-template.ftl";

    private static void cleanupTemplateNode(ServiceRegistry serviceRegistry) {
        String templatePath = "/app:company_home/app:dictionary/app:email_templates";
        NodeRef rootNode = serviceRegistry.getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        List<NodeRef> templatesNodes = serviceRegistry.getSearchService().selectNodes(
                rootNode,
                templatePath,
                null,
                serviceRegistry.getNamespaceService(),
                false
        );

        NodeRef templateParent = templatesNodes.get(0);

        List<ChildAssociationRef> children = serviceRegistry.getNodeService().getChildAssocs(templateParent);

        for (ChildAssociationRef child : children) {
            serviceRegistry.getNodeService().deleteNode(child.getChildRef());
        }
    }

    // This should create the node and get the template noderef
    @Test public void createAndGetTemplate() throws IOException {

        AuthenticationUtil.setRunAsUserSystem();

        TemplateUtilsTestIT.cleanupTemplateNode(getServiceRegistry());

        String filecontent = ClassLoaderUtils.getInBase64(classLoader, "alfresco/extension/templates/freemarker/" + TEST_MODEL_NAME);
        filecontent = new String(Base64.decodeBase64(filecontent));


        // This should create the node
        String noderefStr = TemplateUtils.getTemplateFromName(TEST_MODEL_NAME, getServiceRegistry());

        // We should have the template content
        String templateStr = getServiceRegistry().getTemplateService().processTemplate(noderefStr, new HashMap<>());

        // Template content must be file content
        Assert.assertEquals(filecontent, templateStr);

        // This should not create the node again
        String recreatedNoderefStr = TemplateUtils.getTemplateFromName(TEST_MODEL_NAME, getServiceRegistry());

        // Template content must be file content
        Assert.assertEquals(noderefStr, recreatedNoderefStr);
    }
}
