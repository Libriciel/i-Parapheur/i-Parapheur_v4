<#if daysUntilExpiration??>
Attention, un ou plusieurs certificats cachet serveur arrivent à epxiration dans ${daysUntilExpiration} jour(s) ou moins :
<#else>
Résumé des dates d'expirations des certificats cachet serveur :
</#if>

<#list seals as seal>

    - Certificat : ${seal.title}
    - Issuer : ${seal.description.issuerDN}
    - Date d'expiration : ${seal.description.notAfter?number_to_date?string["dd/MM/yyyy"]}

</#list>