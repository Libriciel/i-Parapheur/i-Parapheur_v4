/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.libriciel.parapheur.crypto.utils;

import fr.libriciel.parapheur.models.DigestAlgorithm;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Classe utilisée pour le chiffrement d'informations privées à l'aide d'une clé sous forme de text
 * (placée en fichier de propriété)
 * <p>
 * Créé par lhameury le 4/18/17.
 */
public class EncryptUtils {

    private static final String CYPHER_AES = "AES";

    private static Logger log = Logger.getLogger(EncryptUtils.class);


    private static @NotNull Key generateKeyFromString(@NotNull final String secKey) throws NoSuchAlgorithmException {

        byte[] key = secKey.getBytes(StandardCharsets.UTF_8);
        MessageDigest sha = MessageDigest.getInstance(DigestAlgorithm.SHA1.toString());
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // use only first 128 bit

        return new SecretKeySpec(key, CYPHER_AES);
    }

    public static @Nullable String encrypt(@NotNull final String valueEnc, @NotNull final String secKey) {
        return encrypt(valueEnc, secKey, CYPHER_AES);
    }

    /**
     * For testing purposes
     */
    static @Nullable String encrypt(@NotNull final String valueEnc, @NotNull final String secKey,
                                    @NotNull final String cypherName) {

        String encryptedVal = null;

        try {
            final Key key = generateKeyFromString(secKey);
            final Cipher c = Cipher.getInstance(cypherName);
            c.init(Cipher.ENCRYPT_MODE, key);
            final byte[] encValue = c.doFinal(valueEnc.getBytes());
            encryptedVal = new BASE64Encoder().encode(encValue);

        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException
                | BadPaddingException | IllegalBlockSizeException e) {
            log.error("EncryptionUtils : encrypt error : " + e.getMessage());
        }

        return encryptedVal;
    }

    public static @Nullable String decrypt(@NotNull final String encryptedValue, @NotNull final String secretKey) {

        String decryptedValue = null;

        try {
            final Key key = generateKeyFromString(secretKey);
            final Cipher c = Cipher.getInstance(CYPHER_AES);
            c.init(Cipher.DECRYPT_MODE, key);
            final byte[] decorVal = new BASE64Decoder().decodeBuffer(encryptedValue);
            final byte[] decValue = c.doFinal(decorVal);
            decryptedValue = new String(decValue);

        } catch (IOException | IllegalBlockSizeException | BadPaddingException | NoSuchPaddingException
                | InvalidKeyException | NoSuchAlgorithmException e) {
            log.error("EncryptionUtils : decrypt error : " + e.getMessage());
        }

        return decryptedValue;
    }
}
