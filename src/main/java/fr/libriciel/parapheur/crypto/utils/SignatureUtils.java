package fr.libriciel.parapheur.crypto.utils;


import fr.libriciel.parapheur.models.DigestAlgorithm;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

public class SignatureUtils {

    private static Logger log = Logger.getLogger(SignatureUtils.class);

    static {
        Security.addProvider(new BouncyCastleProvider());
    }


    /**
     * Calcule le haché d'un fichier et le restitue sous forme de tableau d'octets.
     *
     * @param fileToDigest    le fichier à hacher
     * @param digestAlgorithm l'algorithme de hachage que l'on souhaite utiliser : "SHA1", "MD5", etc...
     * @param bufSize         Taille du buffer de lecture
     * @return le hash du fichier
     */
    static @Nullable byte[] getFileDigest(@NotNull final File fileToDigest,
                                          @NotNull final DigestAlgorithm digestAlgorithm,
                                          final int bufSize) throws IOException {

        FileInputStream givenStream = new FileInputStream(fileToDigest);
        MessageDigest digest;
        byte[] hash = null;
        try {
            digest = MessageDigest.getInstance(digestAlgorithm.toString(), new BouncyCastleProvider());
            final byte[] buffer = new byte[bufSize];
            while (givenStream.read(buffer) != -1) {
                digest.update(buffer);
            }
            givenStream.close();
            hash = digest.digest();
        } catch (NoSuchAlgorithmException e) {
            log.error("Digest error", e);
        }
        return hash;
    }

    /**
     * Calcule le condensat selon l'algo passé en paramètre
     *
     * @param bytesToDigest   not null
     * @param digestAlgorithm "SHA1" ou "SHA256" sont supportés
     * @return le hash du fichier
     */
    public static @Nullable byte[] getBytesDigest(@NotNull final byte[] bytesToDigest,
                                                  @NotNull final DigestAlgorithm digestAlgorithm) {

        MessageDigest digest;
        byte[] hash = null;
        try {
            digest = MessageDigest.getInstance(digestAlgorithm.toString(), new BouncyCastleProvider());
            digest.update(bytesToDigest);
            hash = digest.digest();
        } catch (NoSuchAlgorithmException e) {
            log.error("Digest error", e);
        }

        return hash;
    }

}
