/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 * Contributor Copyright (C) Augustin Sarr - Netheos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.crypto.utils;

import com.google.common.primitives.Bytes;
import org.apache.log4j.Logger;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Store;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.extensions.surf.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;


@SuppressWarnings("unused")
public class PKCS7Utils {

    static final String PEM_HEADER = "-----BEGIN PKCS7-----\n";
    static final String PEM_FOOTER = "\n-----END PKCS7-----\n";

    private static Logger log = Logger.getLogger(PKCS7Utils.class);
    private static char[] map1 = new char[64];
    private static byte[] map2 = new byte[128];

    // <editor-fold name="Static init">

    static {
        int i = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            map1[i++] = c;
        }
        for (char c = 'a'; c <= 'z'; c++) {
            map1[i++] = c;
        }
        for (char c = '0'; c <= '9'; c++) {
            map1[i++] = c;
        }
        map1[i++] = '+';
        map1[i] = '/';
    }

    static {
        for (int i = 0; i < 128; i++) {
            map2[i] = -1;
        }
        for (int i = 0; i < 64; i++) {
            map2[map1[i]] = (byte) i;
        }
    }

    // </editor-fold name="Static init">

    // <editor-fold name="Private utils">

    static int indexOf(byte[] mainArray, byte[] searchSequence, int fromIndex) {

        if (fromIndex > mainArray.length) {
            return -1;
        }

        byte[] truncatedArray = Arrays.copyOfRange(mainArray, fromIndex, mainArray.length);
        int result = Bytes.indexOf(truncatedArray, searchSequence);

        return (result == -1) ? -1 : (result + fromIndex);
    }

    static int indexOf(byte[] mainArray, byte searchByte, int fromIndex) {

        if (fromIndex > mainArray.length) {
            return -1;
        }

        byte[] truncatedArray = Arrays.copyOfRange(mainArray, fromIndex, mainArray.length);
        int result = Bytes.indexOf(truncatedArray, searchByte);

        return (result == -1) ? -1 : (result + fromIndex);
    }

    public static byte[] pem2der(byte[] pem, byte[] header, byte[] footer) {

        int start = Bytes.indexOf(pem, header);
        int end = Bytes.indexOf(pem, footer);

        if (start == -1 || end == -1) {
            // No headers!
            return null;
        }

        start = indexOf(pem, (byte) '\n', start) + 1;

        // skip past any more text, by avoiding all lines less than 64 characters long...
        int next;
        while ((next = indexOf(pem, (byte) '\n', start)) < start + 60) {

            // really shouldn't ever happen...
            if (next == -1) {
                break;
            }
            start = next + 1;
        }

        // something wrong - no end of line after '-----BEGIN...'
        if (start == -1) {
            return null;
        }

        int len = end - start;
        byte[] data = new byte[len];

        // remove the PEM fluff from tbe base 64 data, stick in 'data'
        System.arraycopy(pem, start, data, 0, len);

        return Base64.decode(data);
    }

    public static byte[] der2pem(byte[] der) {
        ByteArrayOutputStream out = new ByteArrayOutputStream(der.length +
                PEM_HEADER.length() + PEM_FOOTER.length());

        try {
            out.write(PEM_HEADER.getBytes());
            out.write(Base64.encodeBytes(der).getBytes());
            out.write(PEM_FOOTER.getBytes());
        } catch (IOException ex) {
            /* Not used */
        }

        return out.toByteArray();
    }

    public static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    private static byte[] decode(char[] in) {
        int iLen = in.length;
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (iLen > 0 && in[iLen - 1] == '=') {
            iLen--;
        }
        int oLen = (iLen * 3) / 4;
        byte[] out = new byte[oLen];
        int ip = 0;
        int op = 0;
        while (ip < iLen) {
            int i0 = in[ip++];
            int i1 = in[ip++];
            int i2 = ip < iLen ? in[ip++] : 'A';
            int i3 = ip < iLen ? in[ip++] : 'A';
            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int b0 = map2[i0];
            int b1 = map2[i1];
            int b2 = map2[i2];
            int b3 = map2[i3];
            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int o0 = (b0 << 2) | (b1 >>> 4);
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
            int o2 = ((b2 & 3) << 6) | b3;
            out[op++] = (byte) o0;
            if (op < oLen) {
                out[op++] = (byte) o1;
            }
            if (op < oLen) {
                out[op++] = (byte) o2;
            }
        }
        return out;
    }

    // </editor-fold name="Private utils">

    // <editor-fold name="Private X509Certificates utils">

    static @Nullable X509Certificate holderToCertificate(@NotNull X509CertificateHolder holder) {

        JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
        converter.setProvider(new BouncyCastleProvider());

        try {
            return converter.getCertificate(holder);
        } catch (CertificateException e) {
            log.error("CertificateHolder cannot be converted to Certificate", e);
            return null;
        }
    }

    static @NotNull List<X509Certificate> holderToCertificate(@Nullable List<X509CertificateHolder> holders,
                                                              @NotNull JcaX509CertificateConverter converter) {

        List<X509Certificate> result = new ArrayList<>();

        if ((holders == null) || holders.isEmpty()) {
            return result;
        }

        for (X509CertificateHolder holder : holders) {
            if (holder != null) {
                try {
                    result.add(converter.getCertificate(holder));
                } catch (CertificateException e) {
                    log.error("CertificateHolder cannot be converted to Certificate", e);
                }
            }
        }

        return result;
    }

    static @Nullable X509CertificateHolder getSignatureCertificateHolder(@NotNull CMSSignedData signed) {

        Store certStore = signed.getCertificates();

        List<SignerInformation> signers = getSignerInformations(signed);
        for (SignerInformation signer : signers) {

            List<X509CertificateHolder> certHolders = getX509CertificateHolders(certStore, signer);
            if (!certHolders.isEmpty()) {
                return certHolders.get(0);
            }
        }

        return null;
    }

    public static @Nullable X509Certificate getSignatureCertificate(@NotNull CMSSignedData signed) {

        X509CertificateHolder holder = getSignatureCertificateHolder(signed);

        if (holder != null) {
            return holderToCertificate(holder);
        }

        return null;
    }

    private static @NotNull List<SignerInformation> getSignerInformations(@NotNull CMSSignedData signed) {

        List<SignerInformation> result = new ArrayList<>();

        SignerInformationStore signers = signed.getSignerInfos();
        Collection coll = signers.getSigners();
        Iterator it = coll.iterator();
        SignerInformation signer;

        while (it.hasNext()) {
            signer = (SignerInformation) it.next();
            result.add(signer);
        }

        return result;
    }

    private static @NotNull List<X509CertificateHolder> getX509CertificateHolders(@NotNull Store certStore,
                                                                                  @NotNull SignerInformation signer) {

        List<X509CertificateHolder> result = new ArrayList<>();

        //noinspection unchecked
        Collection certColl = certStore.getMatches(signer.getSID());

        for (Object aCertColl : certColl) {
            X509CertificateHolder certificateHolder = (X509CertificateHolder) aCertColl;
            result.add(certificateHolder);
        }

        return result;
    }

    // </editor-fold name="Private utils">

    /**
     * Validate a p7 contained following the "whole or nothing rule".
     *
     * @param data        the data upon which the signature is going to be verified
     * @param p7signature p7 signatures container
     * @return true if all signatures are valid, false otherwise.
     */
    static boolean verify(@NotNull byte[] data, @NotNull byte[] p7signature) {
        int[] ret = verifyAndCount(data, p7signature);
        return (ret != null) && (ret[0] == ret[1]);
    }

    /**
     * Validate a p7 contained following the "whole or nothing rule".
     *
     * @param file        the file upon which the signature is sgoing to be verified
     * @param p7signature p7 signatures container
     * @return true if all signatures are valid, false otherwise.
     */
    static boolean verify(@NotNull File file, @NotNull byte[] p7signature) {
        int[] ret = verifyAndCount(file, p7signature);
        return (ret != null) && (ret[0] == ret[1]);
    }

    /**
     * Verify the signatures and return the number of valid signatures
     *
     * @param data        to upon which the signatures are supposed generated
     * @param p7signature pkcs7 signatures container
     * @return an array of two integers, the first integer indicates the number
     * of valid signatures and the second, the number of signatures
     * (valid and invalid ones).
     * It returns null if the signature format is invalid
     */
    private static @Nullable int[] verifyAndCount(@NotNull byte[] data, @NotNull byte[] p7signature) {
        return verifyAndCount(new CMSProcessableByteArray(data), p7signature);
    }

    /**
     * Verify the signatures and return the number of valid signatures
     *
     * @param file        upon which the signatures are supposed generated
     * @param p7signature pkcs7 signatures container
     * @return an array of two integers, the first integer indicates the number
     * of valid signatures and the second, the number of signatures
     * (valid and invalid ones).
     * It returns null if the signature format is invalid
     */
    private static @Nullable int[] verifyAndCount(@NotNull File file, @NotNull byte[] p7signature) {
        return verifyAndCount(new CMSProcessableFile(file), p7signature);
    }

    /**
     * Verify the signatures and return the number of valid signatures
     *
     * @param data        data to process
     * @param p7signature pkcs7 signatures container
     * @return an array of two integers, the first integer indicates the number
     * of valid signatures and the second, the number of signatures
     * (valid and invalid ones).
     * It returns null if the signature format is invalid
     */
    private static @Nullable int[] verifyAndCount(@NotNull CMSProcessable data, @NotNull byte[] p7signature) {

        int[] cnt = {0, 0};

        try {
            Security.addProvider(new BouncyCastleProvider());
            CMSSignedData signed = new CMSSignedData(data, p7signature);
            Store certStore = signed.getCertificates();
            List<SignerInformation> signers = getSignerInformations(signed);
            JcaSimpleSignerInfoVerifierBuilder verifBuilder = new JcaSimpleSignerInfoVerifierBuilder();
            verifBuilder.setProvider(new BouncyCastleProvider());
            JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
            converter.setProvider(new BouncyCastleProvider());

            for (SignerInformation signer : signers) {
                cnt[1] += signers.size();

                List<X509CertificateHolder> holders = getX509CertificateHolders(certStore, signer);
                List<X509Certificate> certs = holderToCertificate(holders, converter);

                for (X509Certificate cert : certs) {

                    // TODO : validate certificate
                    if (signer.verify(verifBuilder.build(cert))) {
                        log.debug("Signature validation success");
                        cnt[0]++;
                    } else {
                        log.info("Signature verification failed");
                    }
                }
            }

            return cnt;

        } catch (OperatorCreationException | CMSException e) {
            log.error("Signature verification error", e);
        }

        return cnt;
    }
}