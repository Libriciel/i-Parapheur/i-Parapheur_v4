/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.models;

/**
 * These are defined at http://aduwiki.extranet.adullact.org/doku.php?id=open_parapheur#code_d_erreurs
 */
public class ParapheurErrorCode {

    // Generic errors

    public static final int UNKNOWN_SERVER_ERROR = 999;

    // Id errors

    public static final int INVALID_DOSSIER_ID = 1001;
    public static final int INVALID_BUREAU_ID = 1002;
    public static final int INVALID_DOCUMENT_ID = 1003;
    public static final int INVALID_ANNOTATION_ID = 1004;
    public static final int INVALID_WORKFLOW_ID = 1005;
    public static final int INVALID_SEAL_CERTIFICATE_ID = 1006;

    // Errors on save/emit (Protocol)

    public static final int XML_DOCUMENTS_FORBIDDEN_BY_TYPE_OR_SUBTYPE = 2001;
    public static final int XML_DOCUMENTS_FORBIDDEN_AS_ANNEXES = 2002;
    public static final int HELIOS_ONLY_ACCEPTS_XML_DOCUMENTS = 2011;
    public static final int ANNEXES_FORBIDDEN_WITH_HELIOS_PROTOCOL = 2012;
    public static final int PADES_ONLY_ACCEPTS_PDF_DOCUMENTS = 2021;

    // Errors on save/emit (Integrity)

    public static final int CANT_RENAME_A_CURRENTLY_USED_WORKFLOW = 2501;
    public static final int INVALID_METADATA_URL = 2601;

    public static final int INVALID_JSON = 2900;
    public static final int INVALID_DOSSIER_JSON = 2901;
    public static final int INVALID_BUREAU_JSON = 2902;
    public static final int INVALID_DOCUMENT_JSON = 2903;
    public static final int INVALID_ANNOTATION_JSON = 2904;
    public static final int INVALID_WORKFLOW_JSON = 2905;
    public static final int INVALID_SEAL_CERTIFICATE_JSON = 2906;

    // Documents errors

    public static final int PDF_PROTECTED = 3001;
    public static final int MAIN_DOCUMENT_IS_MISSING = 3002;
    public static final int DOCUMENT_ALREADY_EXISTS = 3003;
    public static final int INVALID_PDF = 3004;
    public static final int XDOCUMENTS_NOT_ACCEPTED = 3005;

    // TDT errors

    public static final int UNKNOwN_TDT_ERROR = 4000;
    public static final int TDT_NOT_SET_IN_TYPE = 4001;
    public static final int ACTES_IS_NOT_SUPPORTED_IN_SELECTED_TDT = 4002;
    public static final int TDT_CERTIFICATE_ERROR = 4050;
    public static final int TDT_CERTIFICATE_EXPIRED = 4051;


//    // Id errors
//
//    public static final String INVALID_DOSSIER_ID = "Error code " + ParapheurErrorCode.INVALID_DOSSIER_ID + " : Unknown folder Id";
//    public static final String INVALID_BUREAU_ID = "Error code " + ParapheurErrorCode.INVALID_BUREAU_ID + " : Unknown desk id";
//    public static final String INVALID_DOCUMENT_ID = "Error code " + ParapheurErrorCode.INVALID_DOCUMENT_ID + " : Unknown document id";
//    public static final String INVALID_ANNOTATION_ID = "Error code " + ParapheurErrorCode.INVALID_ANNOTATION_ID + " : Unknown annotation id";
//    public static final String INVALID_WORKFLOW_ID = "Error code " + ParapheurErrorCode.INVALID_WORKFLOW_ID + " : Unknown workflow node";
//    public static final String INVALID_SEAL_CERTIFICATE_ID = "Error code " + ParapheurErrorCode.INVALID_SEAL_CERTIFICATE_ID + " : Unknown seal certificate id";
//
//    // Errors on save/emit (Protocol)
//
//    public static final String XML_DOCUMENTS_FORBIDDEN_BY_TYPE_OR_SUBTYPE = "Error code " + ParapheurErrorCode.XML_DOCUMENTS_FORBIDDEN_BY_TYPE_OR_SUBTYPE + " : XML documents are forbidden with this typology";
//    public static final String XML_DOCUMENTS_FORBIDDEN_AS_ANNEXES = "Error code " + ParapheurErrorCode.XML_DOCUMENTS_FORBIDDEN_AS_ANNEXES + " : XML documents are forbidden as annexes";
//    public static final String HELIOS_ONLY_ACCEPTS_XML_DOCUMENTS = "Error code " + ParapheurErrorCode.HELIOS_ONLY_ACCEPTS_XML_DOCUMENTS + " : Helios protocol only accepts XML documents";
//    public static final String ANNEXES_FORBIDDEN_WITH_HELIOS_PROTOCOL = "Error code " + ParapheurErrorCode.ANNEXES_FORBIDDEN_WITH_HELIOS_PROTOCOL + " : Annexes are forbidden with Helios protocol";
//    public static final String PADES_ONLY_ACCEPTS_PDF_DOCUMENTS = "Error code " + ParapheurErrorCode.PADES_ONLY_ACCEPTS_PDF_DOCUMENTS + " : PAdES only accepts PDF documents";
//
//    // Errors on save/emit (Integrity)
//
//    public static final String CANT_RENAME_A_CURRENTLY_USED_WORKFLOW = "Error code " + ParapheurErrorCode.CANT_RENAME_A_CURRENTLY_USED_WORKFLOW + " : The workflow is currently used and can't be renamed";
//    public static final String INVALID_METADATA_URL = "Error code " + ParapheurErrorCode.INVALID_METADATA_URL + " : URL-typed metadata is not a valid URL";
//
//    public static final String INVALID_DOSSIER_JSON = "Error code " + ParapheurErrorCode.INVALID_DOSSIER_JSON + " : Given JSON is not a valid folder";
//    public static final String INVALID_BUREAU_JSON = "Error code " + ParapheurErrorCode.INVALID_BUREAU_JSON + " : Given JSON is not a valid desk";
//    public static final String INVALID_DOCUMENT_JSON = "Error code " + ParapheurErrorCode.INVALID_DOCUMENT_JSON + " : Given JSON is not a valid document";
//    public static final String INVALID_ANNOTATION_JSON = "Error code " + ParapheurErrorCode.INVALID_ANNOTATION_JSON + " : Given JSON is not a valid annotation";
//    public static final String INVALID_WORKFLOW_JSON = "Error code " + ParapheurErrorCode.INVALID_WORKFLOW_JSON + " : Given JSON is not a valid workflow";
//
//    // Documents errors
//
//    public static final String PDF_PROTECTED = "Error code " + ParapheurErrorCode.PDF_PROTECTED + " : The PDF document is protected";
//    public static final String MAIN_DOCUMENT_IS_MISSING = "Error code " + ParapheurErrorCode.MAIN_DOCUMENT_IS_MISSING + " : Main document missing";
//    public static final String DOCUMENT_ALREADY_EXISTS = "Error code " + ParapheurErrorCode.DOCUMENT_ALREADY_EXISTS + " : Document already exists";
//    public static final String INVALID_PDF = "Error code " + ParapheurErrorCode.INVALID_PDF + " : Invalid PDF";
//    public static final String XDOCUMENTS_NOT_ACCEPTED = "Error code " + ParapheurErrorCode.XDOCUMENTS_NOT_ACCEPTED + " : DOCX/XLSX/PPTX are forbidden";
//
//    // TDT errors
//
//    public static final String UNKNOwN_TDT_ERROR = "Error code " + ParapheurErrorCode.UNKNOwN_TDT_ERROR + " : Unknown TDT error";
//    public static final String TDT_NOT_SET_IN_TYPE = "Error code " + ParapheurErrorCode.TDT_NOT_SET_IN_TYPE + " : No TDT set in typology";
//    public static final String ACTES_IS_NOT_SUPPORTED_IN_SELECTED_TDT = "Error code " + ParapheurErrorCode.ACTES_IS_NOT_SUPPORTED_IN_SELECTED_TDT + " : Actes protocol is not supported with selected TDT";
//    public static final String TDT_CERTIFICATE_ERROR = "Error code " + ParapheurErrorCode.TDT_CERTIFICATE_ERROR + " : TDT certifiacte error";
//    public static final String TDT_CERTIFICATE_EXPIRED = "Error code " + ParapheurErrorCode.TDT_CERTIFICATE_EXPIRED + " : TDT certificate expired";

}


