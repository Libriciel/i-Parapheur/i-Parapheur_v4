/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.models;

import org.jetbrains.annotations.NotNull;

public enum SignatureFormat {

    // Le célèbre et universel format de signature détaché CMS/PKCS#7.
    CMS_PKCS7("CMS"),

    // <b>NE PAS UTILISER</b>.<br/> C'est un format pénible, qui necessite de l'horodatage... Bref c'est insupportable!
    CMS_PKCS7_Ain1("CMS-Allin1"),

    //PAdES simple, ISO32000-1, sans fioriture.
    PADES_ISO32000_1("PADES-basic"),

    // XAdES profil EPES enveloppé v1.1.1, pour signature de flux PESv2.
    XADES_EPES_ENV_PESv2("XADES-env"),

    // XAdES profil EPES enveloppé, spécifique aux flux notariés 'DIA'.
    XADES_EPES_ENV_DIA("XADES-env-xpath"),

    // XAdES profil EPES détaché v1.1.1, non utilisé. Pas fiable non plus, car pas testé.
    XADES_EPES_DET_1_1_1("XADES"),

    // XAdES profil EPES détaché v1.3.2, utilisé pour flux ANTS/ECD.
    XADES_EPES_DET_1_3_2("XADES132"),

    // <b>NON SUPPORTE</b>, Format XAdES avec timeStamp. N'existe pas.
    XADES_T_EPES_ENV_1_3_2("XADES-T-env"),

    // Signature brute sha256, compatible 'EBICS-TS'
    PKCS1_SHA256_RSA("PKCS1_SHA256_RSA");

    public final String name;

    SignatureFormat(@NotNull String desc) {
        this.name = desc;
    }

    public static @NotNull SignatureFormat fromString(String sigFormat) throws UnsupportedOperationException {

        switch (sigFormat) {

            case "PKCS#7/single":
                return SignatureFormat.CMS_PKCS7; // "CMS";
            case "PKCS#7/multiple":
                return SignatureFormat.CMS_PKCS7_Ain1; // "CMS-Allin1";
            case "PAdES/basic":
                return SignatureFormat.PADES_ISO32000_1; // "PADES-basic";
            case "PAdES/basicCertifie":
                return SignatureFormat.PADES_ISO32000_1; // "PADES-basic";
            case "XAdES/enveloped":
                return SignatureFormat.XADES_EPES_ENV_PESv2; // "XADES-env";
            case "XAdES/DIA":
                return SignatureFormat.XADES_EPES_ENV_DIA; // "XADES-env-xpath";
            case "XAdES/detached":
                return SignatureFormat.XADES_EPES_DET_1_1_1; // "XADES";
            case "XAdES132/detached":
                return SignatureFormat.XADES_EPES_DET_1_3_2; // "XADES132";
            case "XAdES-T/enveloped":
                return SignatureFormat.XADES_T_EPES_ENV_1_3_2; // "XADES-T-env";
            case "PKCS#1/sha256":
                return SignatureFormat.PKCS1_SHA256_RSA; // "PKCS1_SHA256_RSA";
            default:
                throw new UnsupportedOperationException("Unknown signature format: " + sigFormat);
        }
    }

    @Override public String toString() {
        return name;
    }
}
