package fr.libriciel.parapheur.api.seals;

import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Pour sérialisation JSON
 */
@XmlRootElement(name = "Seal")
public class SealCertificate {

    @XmlElement(name = "id") private String id;
    @XmlElement(name = "title") private String title;
    @XmlElement(name = "description") private CachetDescription description;
    @XmlElement(name = "originalName") private String originalName;
    @XmlElement(name = "certificate") private String certificate;
    @XmlElement(name = "password") private String password;
    @XmlElement(name = "image") private String image;

    public SealCertificate() {
    }

    public SealCertificate(String title, String certificate, String password, CachetDescription description) {
        this.title = title;
        this.certificate = certificate;
        this.password = password;
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    /**
     * The password is deserialized from the web interface,
     * but never retrieved from the database.
     * <p>
     * We won't present a classic "getPassword" method.
     * Thus, this one was renamed for disambiguation.
     *
     * @return the certificate password, if the object was deserialized from a web request ; null if the object is from the DB.
     */
    public @Nullable String getDeserializedPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CachetDescription getDescription() {
        return description;
    }

    public void setDescription(CachetDescription description) {
        this.description = description;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public static class CachetDescription {

        private String alias;
        private String issuerDN;
        private long notAfter;
        private String subjectDN;

        public CachetDescription(String alias, String issuerDN, long notAfter, String subjectDN) {
            this.alias = alias;
            this.issuerDN = issuerDN;
            this.notAfter = notAfter;
            this.subjectDN = subjectDN;
        }

        public CachetDescription() {
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public String getIssuerDN() {
            return issuerDN;
        }

        public void setIssuerDN(String issuerDN) {
            this.issuerDN = issuerDN;
        }

        public long getNotAfter() {
            return notAfter;
        }

        public void setNotAfter(long notAfter) {
            this.notAfter = notAfter;
        }

        public String getSubjectDN() {
            return subjectDN;
        }

        public void setSubjectDN(String subjectDN) {
            this.subjectDN = subjectDN;
        }
    }

    @Override public String toString() {
        return "{SealCertificate id=" + id + " title=" + title + "}";
    }
}
