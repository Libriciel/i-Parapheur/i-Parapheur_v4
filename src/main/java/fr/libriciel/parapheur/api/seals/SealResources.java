/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.api.seals;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import fr.libriciel.parapheur.api.seals.exceptions.NodeRefIsNotSealException;
import fr.libriciel.parapheur.utils.StringUtils;
import io.swagger.annotations.*;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.MalformedNodeRefException;
import org.apache.commons.httpclient.HttpStatus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.extensions.webscripts.WebScriptResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static fr.libriciel.parapheur.models.ParapheurErrorCode.*;


@SuppressWarnings("VoidMethodAnnotatedWithGET")
@Path("/seals")
@Api(value = "/seals", description = "Operations about seals")
public class SealResources {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Adds a new Certificate Seal to the keystore",
                  response = SealCertificate.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpStatus.SC_OK, message = "OK"),
            @ApiResponse(code = HttpStatus.SC_BAD_REQUEST,
                         message = "Error code " + INVALID_SEAL_CERTIFICATE_JSON + " : Given JSON is not a valid certificate <br/>"
                                 + "Error code " + INVALID_JSON + " : Invalid JSON <br/>"
                                 + "Error code " + UNKNOWN_SERVER_ERROR + " : Unknown internal error"),
            @ApiResponse(code = HttpStatus.SC_CONFLICT, message = "A SealCertificate already exists with this id")})
    public static void createSealCert(@NotNull ServiceRegistry serviceRegistry,
                                      @NotNull WebScriptResponse webScriptResponse,
                                      @NotNull String certificatesKey,
                                      @NotNull String sealJson) {

        webScriptResponse.setContentEncoding(StandardCharsets.UTF_8.name());
        webScriptResponse.setContentType(MediaType.APPLICATION_JSON);

        // Parsing input

        final Gson gson = new GsonBuilder().create();
        SealCertificate seal;

        try {
            seal = gson.fromJson(sealJson, SealCertificate.class);
        } catch (JsonSyntaxException e) {
            webScriptResponse.setStatus(HttpStatus.SC_BAD_REQUEST);
            return;
        }

        if (seal == null) {
            webScriptResponse.setStatus(HttpStatus.SC_BAD_REQUEST);
            return;
        }

        // Creating

        try {
            SealCertificate certificate = SealUtils.createSealCertificate(
                    seal,
                    certificatesKey,
                    serviceRegistry
            );

            if (certificate != null) {
                webScriptResponse.setStatus(HttpStatus.SC_OK);
                webScriptResponse.setContentEncoding(StandardCharsets.UTF_8.name());
                webScriptResponse.getWriter().write(gson.toJson(certificate));
            } else {
                webScriptResponse.setStatus(HttpStatus.SC_CONFLICT);
            }
        } catch (IllegalArgumentException | IOException e) {
            webScriptResponse.setStatus(HttpStatus.SC_BAD_REQUEST);
        }
    }

    @GET
    @Path("/{sealId}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Lists the keystore content",
                  response = SealCertificate.class,
                  responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = HttpStatus.SC_OK, message = "OK"),
            @ApiResponse(code = HttpStatus.SC_INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public static void listSealCert(@NotNull ServiceRegistry serviceRegistry, @NotNull WebScriptResponse webScriptResponse) {

        final Gson gson = new GsonBuilder().create();
        webScriptResponse.setContentEncoding(StandardCharsets.UTF_8.name());

        List<SealCertificate> certificatesList = SealUtils.listSealCertificates(serviceRegistry);

        try {
            webScriptResponse.getWriter().write(gson.toJson(certificatesList));
            webScriptResponse.setStatus(HttpStatus.SC_OK);
        } catch (IOException e) {
            webScriptResponse.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @POST
    @Path("/{sealId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Updates seals, overwriting it with given data",
                  response = SealCertificate.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpStatus.SC_OK, message = "OK"),
            @ApiResponse(code = HttpStatus.SC_FORBIDDEN, message = "Invalid id"),
            @ApiResponse(code = HttpStatus.SC_BAD_REQUEST,
                         message = "Error code " + INVALID_SEAL_CERTIFICATE_JSON + " : Given JSON is not a valid certificate <br/>"
                                 + "Error code " + INVALID_JSON + " : Invalid JSON"),
            @ApiResponse(code = HttpStatus.SC_CONFLICT, message = "A SealCertificate already exists with this id"),
            @ApiResponse(code = HttpStatus.SC_INTERNAL_SERVER_ERROR, message = "Unknown server error")})
    public static void updateSealCert(@NotNull ServiceRegistry serviceRegistry,
                                      @NotNull WebScriptResponse webScriptResponse,
                                      @NotNull String certificatesKey,
                                      @PathParam("sealId")
                                      @ApiParam(value = "Target seal certificate", allowableValues = StringUtils.NODE_ID_REGEX, required = true)
                                      @Nullable String sealId,
                                      @NotNull String sealJson) {

        webScriptResponse.setContentEncoding(StandardCharsets.UTF_8.name());
        webScriptResponse.setContentType(MediaType.APPLICATION_JSON);

        // Parsing input

        final Gson gson = new GsonBuilder().create();
        SealCertificate seal;

        try {
            seal = gson.fromJson(sealJson, SealCertificate.class);
        } catch (JsonSyntaxException e) {
            webScriptResponse.setStatus(HttpStatus.SC_BAD_REQUEST);
            return;
        }

        // Integrity checks

        if (seal == null) {
            webScriptResponse.setStatus(HttpStatus.SC_BAD_REQUEST);
            return;
        }

        if (!StringUtils.isValidNodeId(sealId)) {
            webScriptResponse.setStatus(HttpStatus.SC_FORBIDDEN);
            return;
        }

        // Id fix

        if (!StringUtils.equals(sealId, seal.getId())) {
            seal.setId(sealId);
        }

        // Updating

        SealUtils.updateSealCertificate(seal, certificatesKey, serviceRegistry);
        webScriptResponse.setStatus(HttpStatus.SC_OK);

        try {
            webScriptResponse.getWriter().write(gson.toJson(seal));
        } catch (InvalidNodeRefException | IOException e) {
            webScriptResponse.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @DELETE
    @Path("/{sealId}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Removes the certificate from the keystore")
    @ApiResponses(value = {
            @ApiResponse(code = HttpStatus.SC_OK, message = "OK"),
            @ApiResponse(code = HttpStatus.SC_BAD_REQUEST, message = "Error code " + UNKNOWN_SERVER_ERROR + " : Unknown internal error"),
            @ApiResponse(code = HttpStatus.SC_FORBIDDEN,
                         message = "You don't have the rights to modify this resource <br/>"
                                 + "Error code " + INVALID_SEAL_CERTIFICATE_ID + " : The given id is not a seal"),
            @ApiResponse(code = HttpStatus.SC_NOT_FOUND, message = "No resource found for the given id")})
    public static void deleteSealCert(@NotNull ServiceRegistry serviceRegistry,
                                      @NotNull WebScriptResponse webScriptResponse,
                                      @PathParam("sealId")
                                      @ApiParam(value = "Target seal certificate", allowableValues = StringUtils.NODE_ID_REGEX, required = true)
                                      @Nullable String sealId) {

        webScriptResponse.setContentEncoding(StandardCharsets.UTF_8.name());
        int status = HttpStatus.SC_OK;

        try {
            SealUtils.deleteSealCertificate(sealId, serviceRegistry);
        } catch (MalformedNodeRefException e) {
            status = HttpStatus.SC_BAD_REQUEST;
        } catch (InvalidNodeRefException e) {
            status = HttpStatus.SC_NOT_FOUND;
        } catch (NodeRefIsNotSealException e) {
            status = HttpStatus.SC_FORBIDDEN;
        }

        webScriptResponse.setStatus(status);
    }
}
