package fr.libriciel.parapheur.api.seals.exceptions;


import org.alfresco.error.AlfrescoRuntimeException;

/**
 * Exception permettant de relever qu'un noeud n'est pas un "SealCertificate"
 * Créé par lhameury le 4/25/17.
 */
public class NodeRefIsNotSealException extends AlfrescoRuntimeException {
    public NodeRefIsNotSealException() { super("NodeRefIsNotSealException"); }
}
