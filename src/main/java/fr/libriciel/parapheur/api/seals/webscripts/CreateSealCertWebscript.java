/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.api.seals.webscripts;

import fr.libriciel.parapheur.api.seals.SealResources;
import org.alfresco.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;


public class CreateSealCertWebscript extends AbstractWebScript {

    private ServiceRegistry serviceRegistry;
    private String certificatesKey;

    // <editor-fold desc="Constructor, setters, getters">

    @Autowired public CreateSealCertWebscript(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setCertificatesKey(String certificatesKey) {
        this.certificatesKey = certificatesKey;
    }

    // </editor-fold desc="Constructor, setters, getters">

    @Override public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String jsonContent = webScriptRequest.getContent().getContent();
        SealResources.createSealCert(serviceRegistry, webScriptResponse, certificatesKey, jsonContent);
    }
}
