package fr.libriciel.parapheur.api.seals.webscripts;

import fr.libriciel.parapheur.api.seals.SealResources;
import org.alfresco.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;


public class UpdateSealCertWebscript extends AbstractWebScript {

    private final ServiceRegistry serviceRegistry;
    private String certificatesKey;

    // <editor-fold desc="Constructor, setters, getters">

    @Autowired
    public UpdateSealCertWebscript(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setCertificatesKey(String certificatesKey) {
        this.certificatesKey = certificatesKey;
    }

    // </editor-fold desc="Constructor, setters, getters">

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        SealResources.updateSealCert(
                serviceRegistry,
                webScriptResponse,
                certificatesKey,
                webScriptRequest.getServiceMatch().getTemplateVars().get("id"),
                webScriptRequest.getContent().getContent()
        );
    }

}
