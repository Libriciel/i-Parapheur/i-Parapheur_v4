/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.pdf.utils;

import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.parser.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Un TextRenderListener 'custom' pour detecter et localiser une chaine de
 * caracteres dans une page PDF.
 * Merci iText...
 *
 * @author Stephane Vast
 */
class SignatureTagLocatorTextRenderListener implements RenderListener {

    private String signatureTag;
    protected List<Vector> out;
    private Vector startPoint;
    private StringBuffer buffer;
    private boolean isFinding;
    private Pattern patternFix = Pattern.compile("\\s");

    SignatureTagLocatorTextRenderListener(@NotNull String signatureTagString, @NotNull List<Vector> outList) {
        this.signatureTag = signatureTagString;
        this.out = outList;
        this.startPoint = new Vector(0, 0, 1);
        this.isFinding = false;
        this.buffer = new StringBuffer();
    }

    @Override public void renderText(TextRenderInfo renderInfo) {

        if (!isFinding && renderInfo.getText().contains(signatureTag.substring(0, 1))) {
            isFinding = true;
            buffer.setLength(0);
            buffer.append(patternFix.matcher(renderInfo.getText()).replaceAll(""));
            startPoint = renderInfo.getAscentLine().getStartPoint();
            // Sometimes, we get all signature tag in one shot... Sometimes not !
            if (renderInfo.getText().equals(signatureTag)) {
                out.add(pointMilieu(startPoint, renderInfo.getBaseline().getEndPoint()));
                isFinding = false;
            }
        } else if (isFinding) {
            buffer.append(patternFix.matcher(renderInfo.getText()).replaceAll(""));
            if (signatureTag.length() <= buffer.length()) {
                // System.out.println("final breakthrough, buffer is " + buffer.toString());
                if (buffer.toString().toLowerCase().contains(signatureTag)) {
                    // System.out.println("#### HELL YEAH!! Lycos found the Holy '#Signature#' Tag at " + startPoint.toString());
                    out.add(pointMilieu(startPoint, renderInfo.getBaseline().getEndPoint()));
                }
                // on a dépassé le curseur
                isFinding = false;
            } else if (!signatureTag.contains(buffer)) {
                isFinding = false;
                renderText(renderInfo);
            }
        }
    }

    @Override public void beginTextBlock() {
    }

    @Override public void endTextBlock() {
    }

    @Override public void renderImage(ImageRenderInfo renderInfo) {
    }

    private @NotNull Vector pointMilieu(@NotNull Vector start, @NotNull Vector end) {

        float x = (start.get(Vector.I1) + end.get(Vector.I1)) / 2;
        float y = (start.get(Vector.I2) + end.get(Vector.I2)) / 2;
        float z = (start.get(Vector.I3) + end.get(Vector.I3)) / 2;

        return new Vector(x, y, z);
    }
}

