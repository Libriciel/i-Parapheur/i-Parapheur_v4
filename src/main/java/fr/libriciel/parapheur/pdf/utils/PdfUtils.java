/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.pdf.utils;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ContentByteUtils;
import com.itextpdf.text.pdf.parser.PdfContentStreamProcessor;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.Vector;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


class PdfUtils {

    static final String DEFAULT_SIGNATURE_TAG = "#signature#";
    static final Point DEFAULT_SIGNATURE_SIZE = new Point(100, 100);

    private static Logger log = Logger.getLogger(PdfUtils.class);

    /**
     * Inflates x, y, ... of given signature tag.
     *
     * @param reader        : The opened target document
     * @param signatureTag  : {@link PdfUtils#DEFAULT_SIGNATURE_TAG}, usually
     * @param signatureSize : {@link PdfUtils#DEFAULT_SIGNATURE_SIZE}, usually
     * @return A map entry like {page=(x:y)}. The page starts at index 1, not 0.
     * @throws IOException on reader stream problems.
     */
    static @Nullable Map.Entry<Integer, Rectangle> findSignatureTagPosition(@NotNull PdfReader reader,
                                                                            @NotNull String signatureTag,
                                                                            @NotNull Point signatureSize) throws IOException {

        List<Vector> foundPositions = new ArrayList<>();

        int i = 0;
        int currentPage = -1;
        int numberOfPages = reader.getNumberOfPages();

        while ((i < numberOfPages) && foundPositions.isEmpty()) {

            // Some reverse-ordering here, since signatures are often on firsts or last pages...
            // On a loop [0.. 6], it will return [0, 5, 1, 4, 2, 3].
            currentPage = (i % 2 == 0) ? i / 2 : (numberOfPages - (i / 2) - 1);

            // In iText, it's a page number, starting at 1, not an index starting at 0.
            currentPage += 1;

            foundPositions.addAll(scanForSignatureTag(reader, signatureTag, currentPage));
            i++;
        }

        if (!foundPositions.isEmpty()) {

            float tmpX = foundPositions.get(0).get(Vector.I1) - signatureSize.x / 2;
            float tmpY = foundPositions.get(0).get(Vector.I2) - signatureSize.y / 2;

            // Check boundaries

            com.itextpdf.text.Rectangle pageSize = reader.getPageSizeWithRotation(currentPage);

            tmpX = Math.max(tmpX, 0F);
            tmpX = Math.min(tmpX, pageSize.getWidth() - signatureSize.x);
            tmpY = Math.max(tmpY, 0F);
            tmpY = Math.min(tmpY, pageSize.getHeight() - signatureSize.y);

            // Result

            int x = Math.round(tmpX);
            int y = Math.round(tmpY);

            log.debug("findSignatureTagPosition - tag=" + signatureTag + " " + "page=" + currentPage +
                    " x=" + x + " y=" + y + " width=" + signatureSize.x + " height=" + signatureSize.y);

            return new AbstractMap.SimpleEntry<>(currentPage, new Rectangle(x, y, signatureSize.x, signatureSize.y));

        } else {
            log.debug("findSignatureTagPosition - tag \"" + signatureTag + "\" not found");
        }

        return null;
    }

    /**
     * C'est le Lycos du tag de Signature dans une page de PDF.
     * Il hurle sa race s'il en voit un, et il te file ses coordonnees!!  :-)
     *
     * @param reader       : The opened target document
     * @param signatureTag : {@link PdfUtils#DEFAULT_SIGNATURE_TAG}, usually
     * @param currentPage  : The page to check, starting at page 1
     * @return a list of found coordinates, may be empty, never null
     */
    private static @NotNull List<Vector> scanForSignatureTag(@NotNull PdfReader reader,
                                                             @NotNull String signatureTag,
                                                             int currentPage) throws IOException {

        // Il faut remercier publiquement maitre Lowagie, sans qui cette noble tache ne pourrait aboutir.
        // Le truc suivant ne marche pas terrible :
        // page 519 de la doc (edition oct.2010) : TextRenderInfo
        // pour accéder aux objets LineSegment (contiennent la localisation du texte!!!)

        List<Vector> coords = new ArrayList<>();

        RenderListener listener = new SignatureTagLocatorTextRenderListener(signatureTag, coords);
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        PdfDictionary pageDic = reader.getPageN(currentPage);

        PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
        processor.processContent(ContentByteUtils.getContentBytesForPage(reader, currentPage), resourcesDic);

        return coords;
    }
}
