/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 * Original work (C) Bruno Lowagie ("iText in Action - 2nd Edition")
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.pdf.utils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.*;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;

public class PdfSealUtils {

    private static Logger log = Logger.getLogger(PdfSealUtils.class);

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * Manipulates a PDF file src with the file dest as result
     *
     * @param src  the original PDF
     * @param dest the resulting PDF
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws DocumentException
     * @throws FileNotFoundException
     * @throws KeyStoreException
     */
    public static void signPdf(@NotNull String src, @NotNull String dest, @NotNull String p12Path,
                               @NotNull String p12Password, @NotNull String p12AliasPassword, boolean certified,
                               boolean graphic) throws GeneralSecurityException, IOException, DocumentException {

        // Private key and certificate
        KeyStore ks = KeyStore.getInstance("PKCS12");

        log.debug(p12Path + " " + p12Password);
        InputStream is = new FileInputStream(p12Path);
        ks.load(is, p12Password.toCharArray());
        String alias = ks.aliases().nextElement();
        PrivateKey pk = (PrivateKey) ks.getKey(alias, p12AliasPassword.toCharArray());
        Certificate[] chain = ks.getCertificateChain(alias);

        // Reader and stamper

        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = PdfStamper.createSignature(reader, new FileOutputStream(dest), '\0');

        // Appearance

        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        appearance.setSignatureCreator("i-Parapheur");

//        appearance.setVisibleSignature("mySig");
//        appearance.setReason("It's personal.");
//        appearance.setLocation("Foobar");

        if (certified)
            appearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);

        // TODO
//        if (graphic) {
//            appearance.setSignatureGraphic(Image.getInstance(RESOURCE));
//            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
//        }

        // Signature

        ExternalSignature es = new PrivateKeySignature(
                pk,
                DigestAlgorithms.SHA256,
                BouncyCastleProvider.PROVIDER_NAME
        );

        ExternalDigest digest = new BouncyCastleDigest();
        MakeSignature.signDetached(
                appearance,
                digest,
                es,
                chain,
                null,
                null,
                null,
                0,
                CryptoStandard.CADES
        );
    }

}