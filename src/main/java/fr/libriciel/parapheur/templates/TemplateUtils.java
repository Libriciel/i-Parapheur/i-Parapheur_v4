/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.parapheur.templates;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Créé par lhameury le 5/10/17.
 */
public class TemplateUtils {

    private static @NotNull NodeRef createTemplateNode(@NotNull final String templateName,
                                                       @NotNull final NodeRef rootNode,
                                                       @NotNull final ServiceRegistry serviceRegistry) {
        NodeRef createdNode;

        NodeRef dictionary = serviceRegistry.getSearchService().selectNodes(
                rootNode,
                "/app:company_home/app:dictionary/app:email_templates",
                null,
                serviceRegistry.getNamespaceService(),
                false
        ).get(0);

        Map<QName, Serializable> props = new HashMap<>();
        props.put(ContentModel.PROP_NAME, templateName);

        createdNode = serviceRegistry.getNodeService().createNode(
                dictionary,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(serviceRegistry.getNamespaceService().getNamespaceURI("cm"), templateName),
                ContentModel.TYPE_CONTENT,
                props
        ).getChildRef();

        ContentWriter writer = serviceRegistry.getContentService().getWriter(createdNode, ContentModel.PROP_CONTENT, true);
        writer.putContent(TemplateUtils.class.getClassLoader().getResourceAsStream(String.format("alfresco/extension/templates/freemarker/%s", templateName)));

        return createdNode;
    }

    public static @NotNull String getTemplateFromName(@NotNull String templateName, @NotNull ServiceRegistry serviceRegistry) {
        NodeRef templateNode;

        String templatePath = "/app:company_home/app:dictionary/app:email_templates/cm:" + templateName;
        NodeRef rootNode = serviceRegistry.getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        List<NodeRef> templatesNodes = serviceRegistry.getSearchService().selectNodes(
                rootNode,
                templatePath,
                null,
                serviceRegistry.getNamespaceService(),
                false
        );

        if (templatesNodes.size() > 0) {
            templateNode = templatesNodes.get(0);
        } else {
            templateNode = createTemplateNode(templateName, rootNode, serviceRegistry);
        }

        return templateNode != null ? templateNode.toString() : "";
    }

}
