<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>fr.libriciel.parapheur</groupId>
    <artifactId>parapheur</artifactId>
    <version>5.0-SNAPSHOT</version>
    <name>parapheur Platform Jar Module - SDK 3</name>
    <description>Platform JAR Module (to be included in the alfresco.war) - SDK 3</description>
    <packaging>jar</packaging>

    <!-- 
        SDK properties have sensible defaults in the SDK parent,
        but you can override the properties below to use another version.
        For more available properties see the alfresco-sdk-parent POM.
       -->
    <properties>
        <!-- Alfresco Maven Plugin version to use -->
        <alfresco.sdk.version>3.0.0</alfresco.sdk.version>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Alfresco Data directory, which will contain:
                Content Store (i.e. the files we upload)
                Database (i.e. the metadata for the uploaded files)
                Search index (i.e. the indexed content and metadata)
             Configured in alfresco-global.properties with the 'dir.root' property.
                -->
        <alfresco.data.location>${session.executionRootDirectory}/alf_data_dev</alfresco.data.location>

        <!-- Duplicated with alfresco.solrHome in the plugin, we need them out here to do filtering -->
        <solr.home>${alfresco.data.location}/solr</solr.home>
        <solr.model.dir>${solr.home}/alfrescoModels</solr.model.dir>
        <solr.content.dir>${solr.home}/index</solr.content.dir>

        <!-- Properties used in dependency declarations, you don't need to change these -->
        <alfresco.groupId>org.alfresco</alfresco.groupId>

        <!-- Alfresco Platform webapp version, this is the original Alfresco webapp that will be
            customized and then deployed and run by the tomcat maven plugin when
            executing for example $ mvn clean install alfresco:run -->
        <alfresco.platform.version>5.2.e</alfresco.platform.version>
        <!-- Alfresco Share version, so we can bring in correct alfresco-share-services artifact -->
        <alfresco.share.version>5.2.d</alfresco.share.version>

        <!-- Default is to run with a Community edition, change to 'enterprise' if using Enterprise edition -->
        <maven.alfresco.edition>community</maven.alfresco.edition>


        <!-- JRebel Hot reloading of classpath stuff and web resource stuff -->
        <jrebel.version>1.1.6</jrebel.version>

        <!-- Environment to use, Alfresco Maven Plugin will
             copy alfresco-global-*.properties files from this directory, such as src/test/properties/local -->
        <env>local</env>

        <!-- Compile with Java 7, default is 5 -->
        <maven.compiler.source>1.7</maven.compiler.source>
        <maven.compiler.target>1.7</maven.compiler.target>

        <bouncy-castle.version>1.56</bouncy-castle.version>
        <gatling-plugin.version>2.2.4</gatling-plugin.version>
        <scala-maven-plugin.version>3.2.2</scala-maven-plugin.version>
        <swagger-version>1.5.10</swagger-version>
        <swagger-maven-plugin-version>3.1.0</swagger-maven-plugin-version>

    </properties>

    <!-- This will import the dependencyManagement for all artifacts in the selected Alfresco platform.
            NOTE: You still need to define dependencies in your POM,
            but you can omit version as it's enforced by this dependencyManagement.
            NOTE: It defaults to the latest version this SDK pom has been tested with,
            but alfresco version can/should be overridden in your project's pom
    -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>${alfresco.groupId}</groupId>
                <artifactId>alfresco-platform-distribution</artifactId>
                <version>${alfresco.platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>

        <!-- COMPILE PHASE -->

        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-jaxrs</artifactId>
            <version>${swagger-version}</version>
            <scope>compile</scope>
        </dependency>

        <!-- TEST PHASE -->

        <!-- Bring in Alfresco RAD so we get access to AlfrescoTestRunner classes -->
        <dependency>
            <groupId>org.alfresco.maven</groupId>
            <artifactId>alfresco-rad</artifactId>
            <version>${alfresco.sdk.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- Integration tests need httpcomponents to execute tests -->
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.5.2</version>
            <scope>test</scope>
        </dependency>

        <!-- Bring in Spring Context so we can use ApplicationContext, ApplicationContextAware etc -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>3.2.17.RELEASE</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.9.5</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.pdfbox</groupId>
            <artifactId>pdfbox</artifactId>
            <version>1.8.13</version>
            <scope>test</scope>
        </dependency>

        <!-- VERIFY PHASE -->

        <dependency>
            <groupId>io.gatling.highcharts</groupId>
            <artifactId>gatling-charts-highcharts</artifactId>
            <version>${gatling-plugin.version}</version>
            <scope>verify</scope>
        </dependency>

        <!-- INSTALL PHASE -->

        <!-- Following dependencies are needed for compiling Java code in src/main/java;
             <scope>provided</scope> is inherited for each of the following;
             for more info, please refer to alfresco-platform-distribution POM -->
        <!-- The main Alfresco Repo dependency for compiling Java code in src/main/java -->
        <dependency>
            <groupId>${alfresco.groupId}</groupId>
            <artifactId>alfresco-repository</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.bouncycastle</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>javax.activation</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>13.0</version>
        </dependency>

        <dependency>
            <groupId>com.itextpdf</groupId>
            <artifactId>itextpdf</artifactId>
            <version>5.5.10</version>
        </dependency>

        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcpkix-jdk15on</artifactId>
            <version>${bouncy-castle.version}</version>
        </dependency>

        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcmail-jdk15on</artifactId>
            <version>${bouncy-castle.version}</version>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>19.0</version>
        </dependency>

        <dependency>
            <groupId>com.google.http-client</groupId>
            <artifactId>google-http-client</artifactId>
            <version>1.22.0</version>
        </dependency>

        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.0</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>3.0.0.RELEASE</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <profiles>
        <!-- We use those to be able to run a daemon tomcat, without pausing the maven build. -->

        <profile>
            <id>integration-test-profile</id>
            <properties>
                <skipTests>true</skipTests>
                <maven.tomcat.fork>true</maven.tomcat.fork>
            </properties>
        </profile>

        <profile>
            <id>integration-and-unit-test-profile</id>
            <properties>
                <skipTests>false</skipTests>
                <maven.tomcat.fork>true</maven.tomcat.fork>
            </properties>
        </profile>

    </profiles>

    <build>

        <plugins>

            <!--
                The Alfresco Maven Plugin contains all the logic to run the extension
                in an embedded Tomcat with the H2 database.
                -->
            <plugin>
                <groupId>org.alfresco.maven.plugin</groupId>
                <artifactId>alfresco-maven-plugin</artifactId>
                <version>${alfresco.sdk.version}</version>
                <executions>
                    <execution>
                        <id>start-alfresco</id>
                        <goals>
                            <goal>it</goal>
                        </goals>
                        <phase>pre-integration-test</phase>
                    </execution>
                </executions>
                <configuration>
                    <!-- We need the flat file H2 database to run the Repo -->
                    <enableH2>true</enableH2>
                    <!-- This is a platform extension JAR, so we need the platform webapp (alfresco.war) -->
                    <enablePlatform>true</enablePlatform>
                    <!-- Enable Solr so we can use search, our Repo extension probably need search -->
                    <enableSolr>true</enableSolr>
                    <!-- We don't need the share.war if we don't have any UI extensions -->
                    <enableShare>false</enableShare>
                    <!-- Enable the REST API Explorer -->
                    <enableApiExplorer>true</enableApiExplorer>

                    <!--
                       JARs and AMPs that should be overlayed/applied to the Platform/Repository WAR
                       (i.e. alfresco.war)
                       -->
                    <platformModules>
                        <!-- This AMP is needed if we are going to access the platform webapp from a Share webapp -->
                        <!-- Share Services will be ignored if you are on Platform earlier than 5.1 -->
                        <moduleDependency>
                            <groupId>${alfresco.groupId}</groupId>
                            <artifactId>alfresco-share-services</artifactId>
                            <version>${alfresco.share.version}</version>
                            <type>amp</type>
                        </moduleDependency>

                        <!-- Bring in this JAR project, need to be included here, otherwise resources from META-INF
                             will not be loaded, such as the test.html page
                        -->
                        <moduleDependency>
                            <groupId>${project.groupId}</groupId>
                            <artifactId>${project.artifactId}</artifactId>
                            <version>${project.version}</version>
                        </moduleDependency>
                    </platformModules>
                </configuration>
            </plugin>

            <!--
                Build an AMP if 3rd party libs are needed by the extensions
                JARs are the default artifact produced in your modules, if you want to build an amp for each module
                you have to enable this plugin and inspect the src/main/assembly.xml file if you want to customize
                the layout of your AMP. The end result is that Maven will produce both a JAR file and an AMP with your
                module.
            -->
            <!--
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>build-amp-file</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptor>src/main/assembly/amp.xml</descriptor>
                        </configuration>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>org.alfresco.maven.plugin</groupId>
                        <artifactId>alfresco-maven-plugin</artifactId>
                        <version>${alfresco.sdk.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            -->


            <!-- Hot reloading with JRebel -->
            <plugin>
                <groupId>org.zeroturnaround</groupId>
                <artifactId>jrebel-maven-plugin</artifactId>
                <version>${jrebel.version}</version>
                <executions>
                    <execution>
                        <id>generate-rebel-xml</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <!-- For more information about how to configure JRebel plugin see:
                         http://manuals.zeroturnaround.com/jrebel/standalone/maven.html#maven-rebel-xml -->
                    <classpath>
                        <fallback>all</fallback>
                        <resources>
                            <resource>
                                <!--
                                Empty resource element marks default configuration. By
                                default it is placed first in generated configuration.
                                -->
                            </resource>
                        </resources>
                    </classpath>

                    <!--
                      alwaysGenerate - default is false
                      If 'false' - rebel.xml is generated if timestamps of pom.xml and the current rebel.xml file are not equal.
                      If 'true' - rebel.xml will always be generated
                    -->
                    <alwaysGenerate>true</alwaysGenerate>
                </configuration>
            </plugin>

            <!-- Fix crappy Maven behavior -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.0.2</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                    <nonFilteredFileExtensions>
                        <nonFilteredFileExtension>p12</nonFilteredFileExtension>
                        <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>p7s</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ftl</nonFilteredFileExtension>
                        <nonFilteredFileExtension>acp</nonFilteredFileExtension>
                        <nonFilteredFileExtension>svg</nonFilteredFileExtension>
                        <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>doc</nonFilteredFileExtension>
                        <nonFilteredFileExtension>docx</nonFilteredFileExtension>
                        <nonFilteredFileExtension>xls</nonFilteredFileExtension>
                        <nonFilteredFileExtension>xlsx</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ppt</nonFilteredFileExtension>
                        <nonFilteredFileExtension>pptx</nonFilteredFileExtension>
                        <nonFilteredFileExtension>bin</nonFilteredFileExtension>
                        <nonFilteredFileExtension>lic</nonFilteredFileExtension>
                        <nonFilteredFileExtension>swf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>zip</nonFilteredFileExtension>
                        <nonFilteredFileExtension>msg</nonFilteredFileExtension>
                        <nonFilteredFileExtension>jar</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>eot</nonFilteredFileExtension>
                        <nonFilteredFileExtension>woff</nonFilteredFileExtension>
                        <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
                        <nonFilteredFileExtension>css</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ico</nonFilteredFileExtension>
                        <nonFilteredFileExtension>psd</nonFilteredFileExtension>
                        <nonFilteredFileExtension>js</nonFilteredFileExtension>
                    </nonFilteredFileExtensions>
                </configuration>
            </plugin>

            <!-- Jacoco Code coverage -->
            <!-- https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.7.9</version>
                <executions>
                    <execution>
                        <id>default-prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>

                    <execution>
                        <id>default-report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <excludes>
                        <exclude>**/*Webscript*.*</exclude>
                    </excludes>
                </configuration>
            </plugin>


            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.19.1</version>
            </plugin>
            <!-- Runs the integration tests, any class that follows naming convention
                 "**/IT*.java", "**/*IT.java", and "**/*ITCase.java" will be considered an integration test -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>2.19.1</version>
                <executions>
                    <execution>
                        <id>default-integration-test</id>
                        <goals>
                            <goal>integration-test</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Starting Tomcat7 during integration-test -->
            <!-- We're not using any version here, so we can override the internal Alfresco one -->
            <!--<plugin>-->
            <!--<groupId>org.apache.tomcat.maven</groupId>-->
            <!--<artifactId>tomcat7-maven-plugin</artifactId>-->
            <!--<configuration>-->
            <!--<port>8080</port>-->
            <!--<skipErrorOnShutdown>true</skipErrorOnShutdown>-->
            <!--</configuration>-->
            <!--<executions>-->
            <!--<execution>-->
            <!--<id>run-wars</id>-->
            <!--<phase>pre-integration-test</phase>-->
            <!--</execution>-->
            <!--<execution>-->
            <!--<id>shutdown-tomcat</id>-->
            <!--<phase>post-integration-test</phase>-->
            <!--<goals>-->
            <!--<goal>shutdown</goal>-->
            <!--</goals>-->
            <!--</execution>-->
            <!--</executions>-->
            <!--</plugin>-->

            <!-- Gatling Stress tests -->
            <!-- http://gatling.io/ -->
            <!--<plugin>-->
            <!--<groupId>io.gatling</groupId>-->
            <!--<artifactId>gatling-maven-plugin</artifactId>-->
            <!--<version>${gatling-plugin.version}</version>-->
            <!--<executions>-->
            <!--<execution>-->
            <!--<phase>integration-test</phase>-->
            <!--<goals>-->
            <!--<goal>execute</goal>-->
            <!--</goals>-->
            <!--</execution>-->
            <!--</executions>-->
            <!--</plugin>-->

            <plugin>
                <groupId>com.github.kongchen</groupId>
                <artifactId>swagger-maven-plugin</artifactId>
                <version>3.1.1</version>
                <configuration>
                    <apiSources>
                        <apiSource>
                            <springmvc>false</springmvc>
                            <locations>fr.libriciel.parapheur.api</locations>
                            <schemes>https</schemes>
                            <host>secure-iparapheur.collectivite.local</host>
                            <basePath>/parapheur</basePath>
                            <info>
                                <title>iParapheur</title>
                                <version>${version}</version>
                                <description>Public API</description>
                                <termsOfService>https://www.libriciel.fr/i-parapheur</termsOfService>
                                <contact>
                                    <email>iparapheur@libriciel.coop</email>
                                    <name>Libriciel</name>
                                    <url>https://www.libriciel.fr</url>
                                </contact>
                                <license>
                                    <url>https://www.gnu.org/licenses/agpl.html</url>
                                    <name>GNU Affero GPL v3</name>
                                </license>
                            </info>
                            <templatePath>${basedir}/resources/swagger/templates/strapdown.html.hbs</templatePath>
                            <outputPath>${basedir}/target/swagger/document.html</outputPath>
                            <swaggerDirectory>${basedir}/target/swagger/swagger-ui</swaggerDirectory>
                            <!--<securityDefinitions>-->
                            <!--<securityDefinition>-->
                            <!--<json>/securityDefinitions.json</json>-->
                            <!--</securityDefinition>-->
                            <!--</securityDefinitions>-->
                        </apiSource>
                    </apiSources>
                </configuration>
                <executions>
                    <execution>
                        <phase>compile</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>

        <resources>
            <!-- Filter the resource files in this project and do property substitutions -->
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <testResources>
            <!-- Filter the test resource files in this project and do property substitutions -->
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>

    </build>

    <!--
        Alfresco Maven Repositories
        -->
    <repositories>
        <repository>
            <id>alfresco-public</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public</url>
        </repository>
        <repository>
            <id>alfresco-public-snapshots</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public-snapshots</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
        </repository>
        <!-- Alfresco Enterprise Edition Artifacts, put username/pwd for server in settings.xml -->
        <repository>
            <id>alfresco-private-repository</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/private</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>alfresco-plugin-public</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public</url>
        </pluginRepository>
        <pluginRepository>
            <id>alfresco-plugin-public-snapshots</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public-snapshots</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

</project>
