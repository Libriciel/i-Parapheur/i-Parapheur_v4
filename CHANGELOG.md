# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## [Unreleased] - 2017-02-14
### Added
- First commit
- Continuous integration

[Unreleased]: https://gitlab.libriciel.fr/i-parapheur/iparapheur-server/tree/develop